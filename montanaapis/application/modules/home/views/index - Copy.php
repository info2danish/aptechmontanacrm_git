<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo $tot_profile_percentage;exit;
?>
<script src="js/jquery.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="css/jquery-jvectormap.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<!-- SlimScroll -->
<script src="js/jquery.slimscroll.min.js"></script>

<!-- jvectormap  -->
<script src="js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/jquery-jvectormap-world-mill-en.js"></script>
<!-- chart  -->
<script src="js/Chart.js"></script>
<script src="js/dashboard2.js"></script>

<!-- Sparkline -->
<script src="js/jquery.sparkline.min.js"></script>

<!-- start: Content -->
<div id="content" class="content-wrapper">
    <div class="page-title">
      <div>
        <h1>Dashboard</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Dashboard</a></li>
        </ul>
      </div>
    </div>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">CPU Traffic</span>
              <span class="info-box-number">90<small>%</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">41,410</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sales</span>
              <span class="info-box-number">760</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">New Members</span>
              <span class="info-box-number">2,000</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Monthly Recap Report</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <p class="text-center">
                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChart" style="height: 180px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Goal Completion</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Add Products to Cart</span>
                    <span class="progress-number"><b>160</b>/200</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Complete Purchase</span>
                    <span class="progress-number"><b>310</b>/400</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Visit Premium Page</span>
                    <span class="progress-number"><b>480</b>/800</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Send Inquiries</span>
                    <span class="progress-number"><b>250</b>/500</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                    <h5 class="description-header">$35,210.43</h5>
                    <span class="description-text">TOTAL REVENUE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                    <h5 class="description-header">$10,390.90</h5>
                    <span class="description-text">TOTAL COST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                    <h5 class="description-header">$24,813.53</h5>
                    <span class="description-text">TOTAL PROFIT</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                    <h5 class="description-header">1200</h5>
                    <span class="description-text">GOAL COMPLETIONS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Visitors Report</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-9 col-sm-8">
                  <div class="pad">
                    <!-- Map will be created here -->
                    <div id="world-map-markers" style="height: 325px;"></div>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-4">
                  <div class="pad box-pane-right bg-green" style="min-height: 280px">
                    <div class="description-block margin-bottom">
                      <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                      <h5 class="description-header">8390</h5>
                      <span class="description-text">Visits</span>
                    </div>
                    <!-- /.description-block -->
                    <div class="description-block margin-bottom">
                      <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                      <h5 class="description-header">30%</h5>
                      <span class="description-text">Referrals</span>
                    </div>
                    <!-- /.description-block -->
                    <div class="description-block">
                      <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                      <h5 class="description-header">70%</h5>
                      <span class="description-text">Organic</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="row">
            <div class="col-md-6">
              <!-- DIRECT CHAT -->
              <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Direct Chat</h3>

                  <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts"
                            data-widget="chat-pane-toggle">
                      <i class="fa fa-comments"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->
                    <div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="images/user1-128x128.jpg" alt="message user image">
                      <!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        Is this template really for free? That's unbelievable!
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->

                    <!-- Message to the right -->
                    <div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="images/user1-128x128.jpg" alt="message user image">
                      <!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        You better believe it!
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->

                    <!-- Message. Default to the left -->
                    <div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Jan 5:37 pm</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="images/user1-128x128.jpg" alt="message user image">
                      <!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        Working with AdminLTE on a great new app! Wanna join?
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->

                    <!-- Message to the right -->
                    <div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                        <span class="direct-chat-timestamp pull-left">23 Jan 6:10 pm</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="images/user1-128x128.jpg" alt="message user image">
                      <!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        I would love to.
                      </div>
                        
                        
                      <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->

                  </div>
                  <!--/.direct-chat-messages-->

                  <!-- Contacts are loaded here -->
            
                  <!-- /.direct-chat-pane -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <form action="#" method="post">
                    <div class="input-group">
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                      <span class="input-group-btn">
                            <button type="button" class="btn btn-warning btn-flat">Send</button>
                          </span>
                    </div>
                  </form>
                </div>
                <!-- /.box-footer-->
              </div>
              <!--/.direct-chat -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest Members</h3>

                  <div class="box-tools pull-right">
                    <span class="label label-danger">8 New Members</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul class="users-list clearfix">
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">Alexander Pierce</a>
                      <span class="users-list-date">Today</span>
                    </li>
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">Norman</a>
                      <span class="users-list-date">Yesterday</span>
                    </li>
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">Jane</a>
                      <span class="users-list-date">12 Jan</span>
                    </li>
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">John</a>
                      <span class="users-list-date">12 Jan</span>
                    </li>
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">Alexander</a>
                      <span class="users-list-date">13 Jan</span>
                    </li>
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">Sarah</a>
                      <span class="users-list-date">14 Jan</span>
                    </li>
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">Nora</a>
                      <span class="users-list-date">15 Jan</span>
                    </li>
                    <li>
                      <img src="images/user1-128x128.jpg" alt="User Image">
                      <a class="users-list-name" href="#">Nadia</a>
                      <span class="users-list-date">15 Jan</span>
                    </li>
                  </ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="javascript:void(0)" class="uppercase">View All Users</a>
                </div>
                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Orders</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Item</th>
                    <th>Status</th>
                    <th>Popularity</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR9842</a></td>
                    <td>Call of Duty IV</td>
                    <td><span class="label label-success">Shipped</span></td>
                    <td>
                      <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                    </td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR1848</a></td>
                    <td>Samsung Smart TV</td>
                    <td><span class="label label-warning">Pending</span></td>
                    <td>
                      <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
                    </td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR7429</a></td>
                    <td>iPhone 6 Plus</td>
                    <td><span class="label label-danger">Delivered</span></td>
                    <td>
                      <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
                    </td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR7429</a></td>
                    <td>Samsung Smart TV</td>
                    <td><span class="label label-info">Processing</span></td>
                    <td>
                      <div class="sparkbar" data-color="#00c0ef" data-height="20">90,80,-90,70,-61,83,63</div>
                    </td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR1848</a></td>
                    <td>Samsung Smart TV</td>
                    <td><span class="label label-warning">Pending</span></td>
                    <td>
                      <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
                    </td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR7429</a></td>
                    <td>iPhone 6 Plus</td>
                    <td><span class="label label-danger">Delivered</span></td>
                    <td>
                      <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
                    </td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR9842</a></td>
                    <td>Call of Duty IV</td>
                    <td><span class="label label-success">Shipped</span></td>
                    <td>
                      <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4">
          <!-- Info Boxes Style 2 -->
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Inventory</span>
              <span class="info-box-number">5,200</span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
              <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Mentions</span>
              <span class="info-box-number">92,050</span>

              <div class="progress">
                <div class="progress-bar" style="width: 20%"></div>
              </div>
              <span class="progress-description">
                    20% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Downloads</span>
              <span class="info-box-number">114,381</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Direct Messages</span>
              <span class="info-box-number">163,921</span>

              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
              <span class="progress-description">
                    40% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Browser Usage</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="150"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o text-red"></i> Chrome</li>
                    <li><i class="fa fa-circle-o text-green"></i> IE</li>
                    <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                    <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                    <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                    <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#">United States of America
                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                </li>
                <li><a href="#">China
                  <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
              </ul>
            </div>
            <!-- /.footer -->
          </div>
          <!-- /.box -->

          <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Recently Added Products</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <li class="item">
                  <div class="product-img">
                    <img src="images/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Samsung TV
                      <span class="label label-warning pull-right">$1800</span></a>
                    <span class="product-description">
                          Samsung 32" 1080p 60Hz LED Smart HDTV.
                        </span>
                  </div>
                </li>
                 
                <!-- /.item -->
                <li class="item">
                  <div class="product-img">
                    <img src="images/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Bicycle
                      <span class="label label-info pull-right">$700</span></a>
                    <span class="product-description">
                          26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                        </span>
                  </div>
                </li>
                                    

                <!-- /.item -->
                <li class="item">
                  <div class="product-img">
                    <img src="images/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Xbox One <span
                        class="label label-danger pull-right">$350</span></a>
                    <span class="product-description">
                          Xbox One Console Bundle with Halo Master Chief Collection.
                        </span>
                  </div>
                </li>
                  
                <!-- /.item -->
                <li class="item">
                  <div class="product-img">
                    <img src="images/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">PlayStation 4
                      <span class="label label-success pull-right">$399</span></a>
                    <span class="product-description">
                          PlayStation 4 500GB Console (PS4)
                        </span>
                  </div>
                </li>
                <!-- /.item --> 
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="javascript:void(0)" class="uppercase">View All Products</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->    
    
    
    <div class="row">
        <?php if($_SESSION["webadmin"][0]->frecord_id != 0){ ?>
        <?php  
              $res = $this->db->query('Select * from tbl_professional where professional_id = '.$this->db->escape($_SESSION["webadmin"][0]->frecord_id));
              $user_data = '';
              if($res->num_rows()>0){
                $user_data = $res->result_array();
                $user_data = $user_data[0];
              }
              
            ?>
      <?php if(is_array($user_data)){ ?>
		<span style="font-weight:bold;">Profile Completion</span>
		<div class="progress" style="height:20px;" >
		  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $tot_profile_percentage;?>"
		  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $tot_profile_percentage;?>%">
			<?php echo $tot_profile_percentage;?>%
		  </div>
		</div>
		
      <div class="col-sm-12">
          <div class="alert alert-info">
              <?php 
              $image = $user_data['profile_pic'];
              if(empty($image)){
                if($user_data['gender']=='male'){
                  $image = 'default_avatar_male.jpg';
                }else{
                  $image = 'default_avatar_female.jpg';
                }
              }
              $image_path = base_url() . "userfiles/" . $image;
              ?>
              <img src="<?php echo $image_path; ?>" style="width:70px;border-radius: 50%;">&nbsp;&nbsp;
			  
			  
			  
            <span>Hi <?php echo $user_data['first_name'];?>, welcome to Prochat Professional Admin Panel.</span>
            <span style="float: right;margin-top: 2%;">
                <a href="<?php echo base_url();?>professionalprofile/step1"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile</a><br>
                <input id="toggle-event" type="checkbox" <?php if($user_data['is_online']=='yes'){echo 'checked'; }?> data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Online" data-off="Offline" >
            </span>  
            
          </div>
          
		  
		  
      </div>
      <?php }?>
      <div class="col-sm-12"> 
            <form method="post" action="<?php echo node_app; ?>">
                <input type="hidden" name="session_id" value="<?php echo $_SESSION["webadmin"][0]->session_id; ?>">
                <button type="submit" class="btn btn-primary">Open Chat Window</button>
            </form>
      </div>
        <?php }elseif($_SESSION["webadmin"][0]->frecord_id == 0){ ?>
<!--        <div class="col-sm-12"> 
            <form method="post" action="<?php echo node_app; ?>">
                <input type="hidden" name="session_id" value="<?php echo $_SESSION["webadmin"][0]->session_id; ?>">
                <button type="submit" class="btn btn-primary">Open Chat Window</button>
            </form>
      </div>-->
        <?php } ?>
        <div class="clearfix"></div>
        
        <?php if(is_array($ongoing_service) && count($ongoing_service) > 0){?>
        	<div class="card mt20">
        	<div class="services-boxes">
            	<h3 style="text-transform:uppercase">Ongoing Services</h3>
              <?php foreach($ongoing_service as $key => $service){?>
              <div class="col-md-4" style="margin-bottom: 2%;">
                	<div class="border-box">
                	<div class="service-title-names">
                    	<span class="title-names">Service Name:</span>
                        <span class="user-details"><?php echo $service['profservice_name']; ?></span>
                        <div class="clearfix"></div>
                     </div>   
                    <div class="service-title-names">
                    	<span class="title-names">Customer Name:</span>
                        <span class="user-details"><?php echo $service['customer_first_name'].' '.$service['customer_last_name']; ?></span>
                        <div class="clearfix"></div>
                     </div>
                    <div class="service-title-names">
                      <span class="title-names">Days Remaining:</span>
                        <span class="user-details"><?php echo $service['days_remaining'].' '; echo ($service['days_remaining']>1 ? 'Days' : ($service['days_remaining']==0 ? '' : 'Day')); ?></span>    
                        <div class="clearfix"></div>                
                    </div>
                    
                    <div class="recent-messages-wrap">
                        <div class="recent-msg-txt" style="cursor: pointer;"><a href="#">Recent Messages</a></div>
                        <div class="messanger">
                        <div class="messages">
                            <?php if(is_array($service['messages'])){
                              foreach(array_reverse($service['messages']) as $k => $msg){
                                $message = $msg['message_content'];
                                if($msg['message_type'] == 'text'){
                                  $message = $msg['message_content'];
                                }elseif($msg['message_type']=='image'){
                                  $message = '<i class="fa fa-camera" aria-hidden="true"></i> Image';
                                }elseif($msg['message_type']=='video'){
                                  $message = '<i class="fa fa-video-camera" aria-hidden="true"></i> Video';
                                }elseif($msg['message_type']=='file'){
                                  $message = '<i class="fa fa-file" aria-hidden="true"></i> File';
                                }elseif($msg['message_type']=='audio'){
                                  $message = '<i class="fa fa-volume-up" aria-hidden="true"></i> Audio';
                                }
                              ?>
                          <?php if($msg['from_utype']=='prof'){?>
                            <div class="message me">
                                <p class="info" style="width: 90%;"><?php echo $message; ?></p>
                            </div>
                          <?php }else{ ?>
                            <div class="message">
                              <p class="info" style="width: 90%;"><?php echo $message; ?></p>
                            </div>
                          <?php } ?>
                          <?php }} ?>
                        </div>

              </div>
                       <div class="sender  mt20">
                          
                          <form method="post" action="<?php echo node_app; ?>">
                              <input type="hidden" name="cusomer_id" value="<?php echo $service['customer_id']; ?>">
                              <input type="hidden" name="session_id" value="<?php echo $_SESSION["webadmin"][0]->session_id; ?>">
                              <button type="submit" class="btn btn-primary pull-right">Chat Now</button>
                          </form>
                          <div class="clearfix"></div>
                        </div>
                    </div>                    
                    </div>
                </div>
              <?php } ?>
                
                
            </div>
             <div class="clearfix"></div>
        	</div>
        <?php } ?>
        
        <?php if(is_array($completed_service) && count($completed_service) > 0){?>
        	<div class="card mt20">
        	<div class="services-boxes">
            	<h3 style="text-transform:uppercase">Completed Services</h3>
              <?php foreach($completed_service as $key => $service){?>
                <div class="col-md-4" style="margin-bottom: 2%;">
                	<div class="border-box">
                	<div class="service-title-names">
                    	<span class="title-names">Service Name:</span>
                        <span class="user-details"><?php echo $service['profservice_name']; ?></span>
                        <div class="clearfix"></div>
                     </div>   
                    <div class="service-title-names">
                    	<span class="title-names">Customer Name:</span>
                        <span class="user-details"><?php echo $service['customer_first_name'].' '.$service['customer_last_name']; ?></span>
                        <div class="clearfix"></div>
                     </div>
                    
                    
                    <div class="recent-messages-wrap">
                        <div class="recent-msg-txt" style="cursor: pointer;"><a href="#">Last Messages</a></div>
                        <div class="messanger">
                        <div class="messages">
                            <?php if(is_array($service['messages'])){
                              foreach(array_reverse($service['messages']) as $k => $msg){
                                $message = $msg['message_content'];
                                if($msg['message_type'] == 'text'){
                                  $message = $msg['message_content'];
                                }elseif($msg['message_type']=='image'){
                                  $message = '<i class="fa fa-camera" aria-hidden="true"></i> Image';
                                }elseif($msg['message_type']=='video'){
                                  $message = '<i class="fa fa-video-camera" aria-hidden="true"></i> Video';
                                }elseif($msg['message_type']=='file'){
                                  $message = '<i class="fa fa-file" aria-hidden="true"></i> File';
                                }elseif($msg['message_type']=='audio'){
                                  $message = '<i class="fa fa-volume-up" aria-hidden="true"></i> Audio';
                                }
                              ?>
                          <?php if($msg['from_utype']=='prof'){?>
                            <div class="message me">
                                <p class="info" style="width: 90%;"><?php echo $message; ?></p>
                            </div>
                          <?php }else{ ?>
                            <div class="message">
                              <p class="info" style="width: 90%;"><?php echo $message; ?></p>
                            </div>
                          <?php } ?>
                          <?php }} ?>
                        </div>

              </div>
                       <div class="sender mt20">
                          
                          <form method="post" action="<?php echo node_app; ?>">
                              <input type="hidden" name="cusomer_id" value="<?php echo $service['customer_id']; ?>">
                              <input type="hidden" name="session_id" value="<?php echo $_SESSION["webadmin"][0]->session_id; ?>">
                              <button type="submit" class="btn btn-primary pull-right">Read Messages</button>
                          </form>
                          <div class="clearfix"></div>
                        </div>
                    </div>                    
                    </div>
                </div>
              <?php } ?>
               
            </div>
             <div class="clearfix"></div>
        	</div>
        <?php } ?>
          <?php 
              if(is_array($refer_wish_customers_unread_messages) && count($refer_wish_customers_unread_messages) > 0){?>
        	<div class="card mt20">
        	<div class="services-boxes">
            	<h3 style="text-transform:uppercase">Refer & Wish unread messages</h3>
              <?php foreach($refer_wish_customers_unread_messages as $key => $service){?>
                <div class="col-md-4" style="margin-bottom: 2%;">
                	<div class="border-box">
                	<div class="service-title-names">
                    	<span class="title-names">Customer Name:</span>
                        <span class="user-details"><?php echo $service['customer_first_name'].' '.$service['customer_last_name']; ?></span>
                        <div class="clearfix"></div>
                     </div>   
                    <div class="service-title-names">
                    	<span class="title-names">Customer Gender:</span>
                        <span class="user-details"><?php echo $service['gender']; ?></span>
                        <div class="clearfix"></div>
                     </div>
                    <div class="service-title-names">
                      <span class="title-names">Date of Birth:</span>
                      <span class="user-details"><?php echo date('d M, Y',  strtotime($service['dob'])); ?></span>    
                        <div class="clearfix"></div>                
                    </div>
                    
                    <div class="recent-messages-wrap">
                        <div class="recent-msg-txt" style="cursor: pointer;"><a href="#">Recent Messages</a></div>
                        <div class="messanger">
                        <div class="messages">
                            <?php if(is_array($service['messages'])){
                              foreach(array_reverse($service['messages']) as $k => $msg){
                                $message = $msg['message_content'];
                                if($msg['message_type'] == 'text'){
                                  $message = $msg['message_content'];
                                }elseif($msg['message_type']=='image'){
                                  $message = '<i class="fa fa-camera" aria-hidden="true"></i> Image';
                                }elseif($msg['message_type']=='video'){
                                  $message = '<i class="fa fa-video-camera" aria-hidden="true"></i> Video';
                                }elseif($msg['message_type']=='file'){
                                  $message = '<i class="fa fa-file" aria-hidden="true"></i> File';
                                }elseif($msg['message_type']=='audio'){
                                  $message = '<i class="fa fa-volume-up" aria-hidden="true"></i> Audio';
                                }
                              ?>
                          <?php if($msg['from_utype']=='prof'){?>
                            <div class="message me">
                                <p class="info" style="width: 90%;"><?php echo $message; ?></p>
                            </div>
                          <?php }else{ ?>
                            <div class="message">
                              <p class="info" style="width: 90%;"><?php echo $message; ?></p>
                            </div>
                          <?php } ?>
                          <?php }} ?>
                        </div>

              </div>
                       <div class="sender mt20">
                          
                          <form method="post" action="<?php echo node_app; ?>">
                              <input type="hidden" name="cusomer_id" value="<?php echo $service['customer_id']; ?>">
                              <input type="hidden" name="session_id" value="<?php echo $_SESSION["webadmin"][0]->session_id; ?>">
                              <button type="submit" class="btn btn-primary pull-right">Chat Now</button>
                          </form>
                          <div class="clearfix"></div>
                        </div>
                    </div>                    
                    </div>
                </div>
              <?php } ?>
                
                
            </div>
             <div class="clearfix"></div>
        	</div>
        <?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end: Content --> 

<script>
$(document).ready(function(){
	$('.recent-msg-txt').click(function(){
		$(this).next().fadeToggle(100);
	});	
});

$(function() {
    $('#toggle-event').change(function() {
      var status = $(this).prop('checked');
      if(status){
        status='yes';
      }else{
        status='no';
      }
      $.ajax({
				url: "<?php echo base_url()?>home/onlineStatus",
				async: false,
        data:{status:status},
				type: "POST",
        dataType:"json",
				success: function(data){
					//return false;
					
					if(data['status'])
					{
						displayMsg("success",data['msg']);
						
					}
					else
					{
						displayMsg("error","Something Went Worng!");
						setTimeout("location.reload(true);",1000);
					}
				},
        error:function(){
          displayMsg("error","Something Went Worng!");
						setTimeout("location.reload(true);",1000);
        }
                
			});
    })
  })
	
</script>

