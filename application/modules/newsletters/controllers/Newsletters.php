<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Newsletters extends CI_Controller

{
	function __construct()
	{
		parent::__construct();
        //echo "index";exit;
		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);
		
		$this->load->model('newslettersmodel', '', TRUE);
	}
	
	function test_email(){
	    $to_email = "danish.akhtar@attoinfotech.com";			
        $replyto = "info@attoinfotech.website";		
        $subject = "Test Email";
        $headers = "From: info@attoinfotech.website\r\n";
        $headers .= "Reply-To: info@attoinfotech.website\r\n";		
        $headers .= "BCC: infodanish@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";							
        $message  = '<html><body>';		
        $message .= '<p> Testing email...</p>';
        $message .=  '</body></html>';
        
        $mailop = mail($to_email, $subject, $message, $headers);
        
        if($mailop){
        	echo "Email Send";exit;
        }else{
        	echo "Email Not Send";exit;
        }
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->newslettersmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('newsletters/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code*/
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->newslettersmodel->getFormdata($record_id);
			/*echo "<pre>";
			print_r($result['details']);
			exit;
			*/
			
			$student_data = array('action'=>'GetStudentDetailsForNotification', 'CenterID'=>''.$result['details'][0]->center_id.'');
			//$student_data = array('action'=>'GetStudentDetailsForNotification', 'CenterID'=>'2188');
			$getStudentsData = curlFunction(SERVICE_URL, $student_data);
			$getStudentsData = json_decode($getStudentsData, true);
			//echo $getStudentsData['Result'];
			//echo "<pre>";print_r($getStudentsData);exit;
			//if(empty($getStudentsData['Result']) && $getStudentsData['Result'] != 'Failure'){
				$result['students'] = $getStudentsData;
			//}else{
				//$result['students'] = '';
			//}
			
			$this->load->view('template/header.php');
			$this->load->view('newsletters/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	
	/*new code end*/
	function fetch()
	{
		//echo "here..";exit;
		$get_result = $this->newslettersmodel->getRecords($_GET);
		//echo "<pre>";print_r($get_result);exit;
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->newsletter_title);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("NewsLettersAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->newsletter_id . '\',\'' . $get_result['query_result'][$i]->status. '\');" title="Delete">'.$get_result['query_result'][$i]->status .'</a>';
				}	
				
				$actionCol = "";
				if($this->privilegeduser->hasPrivilege("NewsLettersAddEdit")){
					$actionCol.= '<a href="newsletters/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->newsletter_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
				}	
				//$actionCol.= '&nbsp;&nbsp;<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->product_id . '\');" title="Delete"><i class="fa fa-remove"></i></a>';
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol);
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$product_id = "";
			
			if(isset($_POST["product_image"]) && !empty($_POST["product_image"])){
				unset($_POST["product_image"]);
			}
			
			//echo $_SESSION["webadmin"][0]->zone_text;exit;
			$thumnail_value = "";
			
			if(isset($_FILES) && isset($_FILES["cover_image"]["name"]))
			{
				$config['upload_path'] = DOC_ROOT_FRONT."/images/newsletters_images/";
				$config['max_size']    = '1000000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config['file_name']     = md5(uniqid("100_ID", true));
				//$config['file_name']     = $_FILES["cover_image"]["name"].;
				
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("cover_image"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name'];
				}
				
				// For google cloud vision api (image checking)
				$img_path = DOC_ROOT_FRONT."/images/newsletters_images/".$thumnail_value;
				
				//Unlink previous category image
				if(!empty($_POST['newsletter_id']))
				{
					$image = $this->newslettersmodel->getFormdata($_POST['newsletter_id']);
					if(is_array($image) && !empty($image[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->cover_image))
					{
						@unlink(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->cover_image);
					}
				}
				
				//exit;
				
			}
			else
			{
				$thumnail_value = $_POST['input_cover_image'];
			}
			
			$thumnail_value1 = "";
			
			if(isset($_FILES) && isset($_FILES["cover_image2"]["name"]))
			{
				$config['upload_path'] = DOC_ROOT_FRONT."/images/newsletters_images/";
				$config['max_size']    = '1000000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config['file_name']     = md5(uniqid("100_ID", true));
				//$config['file_name']     = $_FILES["cover_image2"]["name"].;
				
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("cover_image2"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value1 = $image_data['upload_data']['file_name'];
				}
				
				// For google cloud vision api (image checking)
				$img_path = DOC_ROOT_FRONT."/images/newsletters_images/".$thumnail_value1;
				
				//Unlink previous category image
				if(!empty($_POST['newsletter_id']))
				{
					$image = $this->newslettersmodel->getFormdata($_POST['newsletter_id']);
					if(is_array($image) && !empty($image[0]->cover_image2) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->cover_image2))
					{
						@unlink(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->cover_image2);
					}
				}
				
				//exit;
				
			}
			else
			{
				$thumnail_value1 = $_POST['input_cover_image2'];
			}
			
			$pdf_value = "";
			if(isset($_FILES) && isset($_FILES["newsletter_pdf"]["name"]))
			{
				$config1['upload_path'] = DOC_ROOT_FRONT."/images/newsletters_images/";
				$config1['max_size']    = '1000000';
				$config1['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config1['file_name']     = md5(uniqid("100_ID", true));
				//$config1['file_name']     = $_FILES["newsletter_pdf"]["name"].;
				
				$this->load->library('upload', $config1);
				if (!$this->upload->do_upload("newsletter_pdf"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$pdf_value = $image_data['upload_data']['file_name'];
				}
				
				
				/* Unlink previous category image */
				if(!empty($_POST['newsletter_id']))
				{
					$image = $this->newslettersmodel->getFormdata($_POST['newsletter_id']);
					if(is_array($image) && !empty($image[0]->newsletter_pdf) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->newsletter_pdf))
					{
						@unlink(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->newsletter_pdf);
					}
				}
				
				//exit;
			}
			else
			{
				$pdf_value = $_POST['input_pdf_file'];
			}
			
			$newsletter_pdf2 = "";
			if(isset($_FILES) && isset($_FILES["newsletter_pdf2"]["name"]))
			{
				$config1['upload_path'] = DOC_ROOT_FRONT."/images/newsletters_images/";
				$config1['max_size']    = '1000000';
				$config1['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config1['file_name']     = md5(uniqid("100_ID", true));
				//$config1['file_name']     = $_FILES["newsletter_pdf2"]["name"].;
				
				$this->load->library('upload', $config1);
				if (!$this->upload->do_upload("newsletter_pdf2"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$newsletter_pdf2 = $image_data['upload_data']['file_name'];
				}
				
				
				/* Unlink previous category image */
				if(!empty($_POST['newsletter_id']))
				{
					$image = $this->newslettersmodel->getFormdata($_POST['newsletter_id']);
					if(is_array($image) && !empty($image[0]->newsletter_pdf2) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->newsletter_pdf2))
					{
						@unlink(DOC_ROOT_FRONT."/images/newsletters_images/".$image[0]->newsletter_pdf2);
					}
				}
				
				//exit;
			}
			else
			{
				$newsletter_pdf2 = $_POST['input_pdf_file2'];
			}
			
			//exit;
			$result = "";
			if (!empty($_POST['newsletter_id'])) {
				$data_array=array();			
				$newsletter_id = $_POST['newsletter_id'];
		 		
				
				$data_array['newsletter_title'] = (!empty($_POST['newsletter_title'])) ? $_POST['newsletter_title'] : '';
				$data_array['cover_image_title'] = (!empty($_POST['cover_image_title'])) ? $_POST['cover_image_title'] : '';
				$data_array['cover_image2_title'] = (!empty($_POST['cover_image2_title'])) ? $_POST['cover_image2_title'] : '';
				$data_array['newsletter_pdf_title'] = (!empty($_POST['newsletter_pdf_title'])) ? $_POST['newsletter_pdf_title'] : '';
				$data_array['newsletter_pdf2_title'] = (!empty($_POST['newsletter_pdf2_title'])) ? $_POST['newsletter_pdf2_title'] : '';
				$data_array['newsletter_datetime'] = (!empty($_POST['newsletter_datetime'])) ? date("Y-m-d H:i:s",strtotime($_POST['newsletter_datetime'])) : '';
				$data_array['newsletter_description'] = (!empty($_POST['newsletter_description'])) ? htmlentities($_POST['newsletter_description']) : '';
				$data_array['cover_image'] = $thumnail_value;
				$data_array['newsletter_pdf'] = $pdf_value;
				$data_array['cover_image2'] = $thumnail_value1;
				$data_array['newsletter_pdf2'] = $newsletter_pdf2;
				$data_array['updated_by']=$_SESSION["webadmin"][0]->user_id;
				
				$result = $this->newslettersmodel->updateRecord('tbl_newsletters', $data_array,'newsletter_id',$newsletter_id);
				
		
			}else {
				
				$data_array=array();
				$data_array['newsletter_title'] = (!empty($_POST['newsletter_title'])) ? $_POST['newsletter_title'] : '';
				$data_array['cover_image_title'] = (!empty($_POST['cover_image_title'])) ? $_POST['cover_image_title'] : '';
				$data_array['cover_image2_title'] = (!empty($_POST['cover_image2_title'])) ? $_POST['cover_image2_title'] : '';
				$data_array['newsletter_pdf_title'] = (!empty($_POST['newsletter_pdf_title'])) ? $_POST['newsletter_pdf_title'] : '';
				$data_array['newsletter_pdf2_title'] = (!empty($_POST['newsletter_pdf2_title'])) ? $_POST['newsletter_pdf2_title'] : '';
				$data_array['newsletter_datetime'] = (!empty($_POST['newsletter_datetime'])) ? date("Y-m-d H:i:s",strtotime($_POST['newsletter_datetime'])) : '';
				$data_array['newsletter_description'] = (!empty($_POST['newsletter_description'])) ? htmlentities($_POST['newsletter_description']) : '';
				$data_array['cover_image'] = $thumnail_value;
				$data_array['newsletter_pdf'] = $pdf_value;
				$data_array['cover_image2'] = $thumnail_value1;
				$data_array['newsletter_pdf2'] = $newsletter_pdf2;
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->newslettersmodel->insertData('tbl_newsletters', $data_array, '1');
				
			}
		
		
			$this->load->library('upload');
			$files = $_FILES;
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	
	function submitEmailForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			if(empty($_POST["students"])){
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Please select atleast one email id.'
				));
				exit;
			}
			
			
			if(!empty($_POST['students'])){
				$details = $this->newslettersmodel->getFormdata($_POST["newsletter_id"]);
				//echo $details[0]->newsletter_description;exit;
				//echo "<pre>";print_r($details);exit;
				$getImages = $this->newslettersmodel->getImages($_POST["newsletter_id"]);
				//echo "<pre>";print_r($getImages);exit;
				for($i=0; $i < sizeof($_POST['students']); $i++){
					//echo $_POST['students'][$i];
					
					$message = "";
					$message = html_entity_decode($details[0]->newsletter_description);
					
					$this->email->clear();
					$this->email->from("mobileapp@aptech.ac.in"); // change it to yours
					$subject = "";
					$subject = "Montana Newsletter - ".$details[0]->newsletter_title;
					$this->email->to($_POST['students'][$i]); // change it to yours
					$this->email->subject($subject);
					$this->email->message($message);
					
					if(!empty($details[0]->cover_image)){
						$filePath = "";
						$filePath = DOC_ROOT_FRONT."/images/newsletters_images/".$details[0]->cover_image;
						$this->email->attach($filePath);
					}

					if(!empty($details[0]->newsletter_pdf)){
						$pdfPath = "";
						$pdfPath = DOC_ROOT_FRONT."/images/newsletters_images/".$details[0]->newsletter_pdf;
						$this->email->attach($pdfPath);
					}
					
					/*if(!empty($getImages)){
						for($j=0; $j < sizeof($getImages); $j++){
							$filePath = "";
							$filePath = DOC_ROOT_FRONT."/images/newsletters_images/".$getImages[$j]->imagename;
							//echo $filePath."<br/>";
							$this->email->attach($filePath);
						}
					}*/
					
					$checkemail = $this->email->send();
					//$headers = "From: contact@attoinfotech.website" . "\r\n" ."CC: danish.akhtar@attoinfotech.com";
					//mail("infodanish@gmail.com","My subject",$message,$headers);
					$this->email->clear(TRUE);
				}
				//exit;
				$result = 1;
			}else{
				$result = "";
			}
			
			
			//exit;
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Email Send Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && newsletter_id='".$_GET['id']."' ";
		$result['details'] = $this->newslettersmodel->getdata('tbl_newsletters',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('newsletters/changeapproval',$result,true);
		echo $view;
	}
	
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		//$config['file_name']     = md5(uniqid("100_ID", true));
		$config['upload_path'] = DOC_ROOT_FRONT."/images/newsletters_images";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}

	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->newslettersmodel->delrecord("tbl_newsletters","newsletter_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->newslettersmodel->delrecord12("tbl_newsletters","newsletter_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	function setDefaultProductImage()
	{
		$image_id = (int)$_POST['image_id'];
		$productImage = array(
							'default' => '0',
						);
		$result = $this->newslettersmodel->updateRecord("tbl_newsletters_images",$productImage,"newsletters_image_id", $image_id);
	}
	
	function deleteProductImage()
	{
		$image_id = (int)$_POST['image_id'];
		$this->newslettersmodel->delrecord1("tbl_newsletters_images","newsletters_image_id",$image_id);	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
	
}

?>
