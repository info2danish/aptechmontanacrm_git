<?php 
error_reporting(0);
$arrayYesNo = array('Yes'=>'Yes','No'=>'No');
$arrayActive = array('Active'=>'Active','Inactive'=>'Inactive','SoldOut'=>'Sold Out');
//echo $_SESSION["webadmin"][0]->role_id;exit;
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Newsletters</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>newsletters">Newsletters</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="newsletter_id" name="newsletter_id" value="<?php if(!empty($details[0]->newsletter_id)){echo $details[0]->newsletter_id;}?>" />
								
								<div class="control-group form-group">
									<label class="control-label"><span>Title*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Title" id="newsletter_title" name="newsletter_title" value="<?php if(!empty($details[0]->newsletter_title)){echo $details[0]->newsletter_title;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Date & Time*</span></label>
									<div class="controls">
										
										
										<input type="text" class="input-xlarge form-control datepicker required" placeholder="Select date & time" id="newsletter_datetime" name="newsletter_datetime" value="<?php echo(isset($details[0]->newsletter_datetime) && $details[0]->newsletter_datetime != "0000-00-00 00:00:00") ? $details[0]->newsletter_datetime : "";?>" format = "dd/MM/yyyy hh:mm:ss">
										
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Cover Image 1 Title</span></label>
									<div class="controls">
										<input type="text" class="form-control" placeholder="Enter Title" id="cover_image_title" name="cover_image_title" value="<?php if(!empty($details[0]->cover_image_title)){echo $details[0]->cover_image_title;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="cover_image">Cover Image 1</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_cover_image" value="<?php if(!empty($details[0]->cover_image)){echo $details[0]->cover_image;}?>" name="input_cover_image" type="hidden" >
										
										<input class="input-xlarge" id="cover_image" name="cover_image" type="file">
										Only (gif, jpg, png, jpeg) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$details[0]->cover_image))
										{
										?>
											<img style="width: 150px;height:150px;padding-top:5px;" src="<?php echo FRONT_URL; ?>/images/newsletters_images/<?php echo $details[0]->cover_image; ?>">
											</img>
											
										<?php 
										}
										?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Cover Image 2 Title</span></label>
									<div class="controls">
										<input type="text" class="form-control" placeholder="Enter Title" id="cover_image2_title" name="cover_image2_title" value="<?php if(!empty($details[0]->cover_image2_title)){echo $details[0]->cover_image2_title;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="cover_image">Cover Image 2</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_cover_image2" value="<?php if(!empty($details[0]->cover_image2)){echo $details[0]->cover_image2;}?>" name="input_cover_image2" type="hidden" >
										
										<input class="input-xlarge" id="cover_image2" name="cover_image2" type="file">
										Only (gif, jpg, png, jpeg) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->cover_image2) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$details[0]->cover_image2))
										{
										?>
											<img style="width: 150px;height:150px;padding-top:5px;" src="<?php echo FRONT_URL; ?>/images/newsletters_images/<?php echo $details[0]->cover_image2; ?>">
											</img>
											
										<?php 
										}
										?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>PDF File 1 Title</span></label>
									<div class="controls">
										<input type="text" class="form-control" placeholder="Enter Title" id="newsletter_pdf_title" name="newsletter_pdf_title" value="<?php if(!empty($details[0]->newsletter_pdf_title)){echo $details[0]->newsletter_pdf_title;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="newsletter_pdf">PDF File 1</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_pdf_file" value="<?php if(!empty($details[0]->newsletter_pdf)){echo $details[0]->newsletter_pdf;}?>" name="input_pdf_file" type="hidden" >
										
										<input class="input-xlarge" id="newsletter_pdf" name="newsletter_pdf" type="file">
										Only (pdf) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->newsletter_pdf) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$details[0]->newsletter_pdf))
										{
										?>
											<a href="<?php echo FRONT_URL; ?>/images/newsletters_images/<?php echo $details[0]->newsletter_pdf; ?>" target="_blank">View PDF</a>
											
										<?php 
										}
										?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>PDF File 2 Title</span></label>
									<div class="controls">
										<input type="text" class="form-control" placeholder="Enter Title" id="newsletter_pdf2_title" name="newsletter_pdf2_title" value="<?php if(!empty($details[0]->newsletter_pdf2_title)){echo $details[0]->newsletter_pdf2_title;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="newsletter_pdf">PDF File 2</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_pdf_file2" value="<?php if(!empty($details[0]->newsletter_pdf2)){echo $details[0]->newsletter_pdf2;}?>" name="input_pdf_file2" type="hidden" >
										
										<input class="input-xlarge" id="newsletter_pdf2" name="newsletter_pdf2" type="file">
										Only (pdf) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->newsletter_pdf2) && file_exists(DOC_ROOT_FRONT."/images/newsletters_images/".$details[0]->newsletter_pdf2))
										{
										?>
											<a href="<?php echo FRONT_URL; ?>/images/newsletters_images/<?php echo $details[0]->newsletter_pdf2; ?>" target="_blank">View PDF</a>
											
										<?php 
										}
										?>
									</div>
								</div>
				
								<div class="control-group form-group">
									<label class="control-label"><span>Contents</span></label>
									<br/>(Editor is for text contens only kindly don't put any image)
									<div class="controls">
										<textarea name="newsletter_description" id="newsletter_description" placeholder="Enter Contents" class="form-control editor"><?php if(!empty($details[0]->newsletter_description)){echo $details[0]->newsletter_description;}?></textarea>
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>newsletters" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	
	$(".datepicker").datetimepicker({
		format: 'YYYY-MM-DD H:m:s'
	});
	

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});


$.validator.addMethod('filesize', function (value, element, param) {
	//alert(element.files[0].size / 1024);return false;
    return this.optional(element) || (element.files[0].size <= 2097152)
}, 'File size must be less than {0}mb');

var vRules = {
	zone_text:{required:true},
	region_text:{required:true},
	area_text:{required:true},
	center_id:{required:true},
	newsletter_title:{required:true, alphanumericwithspace:true},
	newsletter_description:{required:true},
	cover_image:{extension: "gif,jpg,png,jpeg",filesize: 2},
	cover_image2:{extension: "gif,jpg,png,jpeg",filesize: 2}
	
};
var vMessages = {
	zone_text:{required:"Please select zone."},
	region_text:{required:"Please select region."},
	area_text:{required:"Please select area."},
	center_id:{required:"Please select center."},
	newsletter_title:{required:"Please enter title."},
	newsletter_description:{required:"Please enter contents."}
	
};

$("#form-validate").validate({
	success: function(error) { 
        error.removeClass("error");  // <- no, no, no!!
    },
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>newsletters/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>newsletters";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "AddEdit - Newsletter";

 
</script>					
