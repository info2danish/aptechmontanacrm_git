<?php 
error_reporting(0);
$arrayYesNo = array('Yes'=>'Yes','No'=>'No');
$arrayActive = array('Active'=>'Active','Inactive'=>'Inactive','SoldOut'=>'Sold Out');

?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Album</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>albums">Albums</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="album_id" name="album_id" value="<?php if(!empty($details[0]->album_id)){echo $details[0]->album_id;}?>" />
							
							
								<div class="control-group form-group">
									<label class="control-label"><span>Album Name*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Album Name" id="album_name" name="album_name" value="<?php if(!empty($details[0]->album_name)){echo $details[0]->album_name;}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>albums" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->newsletter_id)){
	?>
		//getRegion('<?php echo $details[0]->zone_text; ?>', '<?php echo $details[0]->region_text; ?>');
		//getArea('<?php echo $details[0]->region_text; ?>', '<?php echo $details[0]->area_text; ?>');
		//getCenter('<?php echo $details[0]->area_text; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>
	
});

var vRules = {
	album_name:{required:true, alphanumericwithspace:true}
	
};
var vMessages = {
	album_name:{required:"Please enter valid album name."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>albums/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>albums";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Album";

 
</script>					
