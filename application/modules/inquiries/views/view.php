<?php 
/*echo "<pre>";
print_r($result);
exit;*/
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Inquiry Details</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>inquiries"></a>Inquiries</li>
        </ul>
      </div>
    </div>    
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
            	<div class="col-sm-8 col-md-4">
               		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                    	<input type="hidden" id="inquiry_master_id" name="inquiry_master_id" value="<?php if(!empty($details[0]->inquiry_master_id)){echo $details[0]->inquiry_master_id;}?>" />
						
					<div class="control-group form-group">
						<label class="control-label" for="feedback_reply">Inquiry Status*</label>
						<div class="controls">
							<select name="inquiry_status" id="inquiry_status" class="form-control required">
								<option value="">Select Status</option>
								<option value="Hot" <?php if(!empty($details[0]->inquiry_status) && $details[0]->inquiry_status == 'Hot'){?> selected <?php }?> >Hot</option>
								<option value="Cold" <?php if(!empty($details[0]->inquiry_status) && $details[0]->inquiry_status == 'Cold'){?> selected <?php }?> >Cold</option>
								<option value="Warm" <?php if(!empty($details[0]->inquiry_status) && $details[0]->inquiry_status == 'Warm'){?> selected <?php }?> >Warm</option>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="inquiry_progress">Inquiry Progress*</label>
						<div class="controls">
							<select name="inquiry_progress" id="inquiry_progress" class="form-control required">
								<option value="">Select Status</option>
								<option value="Meeting" <?php if(!empty($details[0]->inquiry_progress) && $details[0]->inquiry_progress == 'Meeting'){?> selected <?php }?> >Meeting</option>
								<option value="Call" <?php if(!empty($details[0]->inquiry_progress) && $details[0]->inquiry_progress == 'Call'){?> selected <?php }?> >Call</option>
								<option value="Walk inn" <?php if(!empty($details[0]->inquiry_progress) && $details[0]->inquiry_progress == 'Walk inn'){?> selected <?php }?> >Walk inn</option>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="zone_text">Academic Year</label>
						<div class="controls">
							<?php if(!empty($details[0]->academic_year_master_name)){ echo $details[0]->academic_year_master_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="zone_text">Zone</label>
						<div class="controls">
							<?php if(!empty($details[0]->zone_name)){ echo $details[0]->zone_name; }?>
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="travel_dob">Center</label>
						<div class="controls">
							<?php if(!empty($details[0]->center_name)){ echo $details[0]->center_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="region_text">Category</label>
						<div class="controls">
							<?php if(!empty($details[0]->categoy_name)){ echo $details[0]->categoy_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="area_text">Course</label>
						<div class="controls">
							<?php if(!empty($details[0]->course_name)){ echo $details[0]->course_name; }?>
						</div>
					</div>
					
						
					<div class="control-group form-group">
						<label class="control-label" for="inquiry_no">Inquiry No.</label>
						<div class="controls">
							<?php if(!empty($details[0]->inquiry_no)){ echo $details[0]->inquiry_no; }?>
						</div>
					</div>
						
					
					
					<div class="control-group form-group">
						<label class="control-label" for="inquiry_date">Inquiry Date</label>
						<div class="controls">
							<?php if(!empty($details[0]->inquiry_date)){ echo date("d-m-Y", strtotime($details[0]->inquiry_date)); }?>
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="student_first_name">Student First Name</label>
						<div class="controls">
							<?php if(!empty($details[0]->student_first_name)){ echo $details[0]->student_first_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="student_last_name">Student Last Name</label>
						<div class="controls">
							<?php if(!empty($details[0]->student_last_name)){ echo $details[0]->student_last_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="student_last_name">Student DOB</label>
						<div class="controls">
							<?php if(!empty($details[0]->student_dob)){ echo date("d-m-Y", strtotime($details[0]->student_dob)); }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="has_attended_preschool_before">Has Attended Preschool?</label>
						<div class="controls">
							<?php if(!empty($details[0]->has_attended_preschool_before)){ echo $details[0]->has_attended_preschool_before; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="preschool_name">Preschool Name</label>
						<div class="controls">
							<?php if(!empty($details[0]->preschool_name)){ echo $details[0]->preschool_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="present_address">Present Address</label>
						<div class="controls">
							<?php if(!empty($details[0]->present_address)){ echo $details[0]->present_address; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="country_name">Country</label>
						<div class="controls">
							<?php if(!empty($details[0]->country_name)){ echo $details[0]->country_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="state_name">State</label>
						<div class="controls">
							<?php if(!empty($details[0]->state_name)){ echo $details[0]->state_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="city_name">City</label>
						<div class="controls">
							<?php if(!empty($details[0]->city_name)){ echo $details[0]->city_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="pincode">Pincode</label>
						<div class="controls">
							<?php if(!empty($details[0]->pincode)){ echo $details[0]->pincode; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="father_name">Father Name</label>
						<div class="controls">
							<?php if(!empty($details[0]->father_name)){ echo $details[0]->father_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="father_contact_no">Father Contact No</label>
						<div class="controls">
							<?php if(!empty($details[0]->father_contact_no)){ echo $details[0]->father_contact_no; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="father_emailid">Father Email ID</label>
						<div class="controls">
							<?php if(!empty($details[0]->father_emailid)){ echo $details[0]->father_emailid; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="father_education">Father Education</label>
						<div class="controls">
							<?php if(!empty($details[0]->father_education)){ echo $details[0]->father_education; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="father_profession">Father Profession</label>
						<div class="controls">
							<?php if(!empty($details[0]->father_profession)){ echo $details[0]->father_profession; }?>
						</div>
					</div>
					<div class="control-group form-group">
						<label class="control-label" for="mother_name">Mother Name</label>
						<div class="controls">
							<?php if(!empty($details[0]->mother_name)){ echo $details[0]->mother_name; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="mother_contact_no">Mother Contact No</label>
						<div class="controls">
							<?php if(!empty($details[0]->mother_contact_no)){ echo $details[0]->mother_contact_no; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="mother_emailid">Mother Email Id</label>
						<div class="controls">
							<?php if(!empty($details[0]->mother_emailid)){ echo $details[0]->mother_emailid; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="mother_education">Mother Education</label>
						<div class="controls">
							<?php if(!empty($details[0]->mother_education)){ echo $details[0]->mother_education; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="mother_profession">Mother Profession</label>
						<div class="controls">
							<?php if(!empty($details[0]->mother_profession)){ echo $details[0]->mother_profession; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="how_know_about_school">How Know About School</label>
						<div class="controls">
							<?php if(!empty($details[0]->how_know_about_school)){ echo $details[0]->how_know_about_school; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="how_know_about_other">Other Source</label>
						<div class="controls">
							<?php if(!empty($details[0]->how_know_about_other)){ echo $details[0]->how_know_about_other; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="inquiry_remark">Inquiry Remark</label>
						<div class="controls">
							<?php if(!empty($details[0]->inquiry_remark)){ echo nl2br($details[0]->inquiry_remark); }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="inquiry_handled_by">Inquiry Handled By</label>
						<div class="controls">
							<?php if(!empty($details[0]->inquiry_handled_by)){ echo $details[0]->inquiry_handled_by; }?>
						</div>
					</div>
					
						
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>inquiries" class="btn btn-primary">Back To Listing</a>
					</div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
         </div>              
    </div>
</div><!-- end: Content -->			
<script>

$(function(){
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						['Source', 'Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
						['Find','Replace','-','Subscript','Superscript'],
						['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['BidiLtr', 'BidiRtl' ],
						['Maximize', 'ShowBlocks'],['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
});


var vRules = {
	inquiry_status:{required:true},
	inquiry_progress:{required:true}
};
var vMessages = {
	inquiry_status:{required:"Please select status."},
	inquiry_progress:{required:"Please select progress."}
};



$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>inquiries/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false, 
			beforeSubmit : function(arr, $form, options){
				$(".btn-primary").hide();
				//return false;
			},
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					$(".btn-primary").show();
					setTimeout(function(){
						window.location = "<?php echo base_url();?>inquiries";
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					$(".btn-primary").show();
					return false;
				}
				$(".btn-primary").show();
			}
		});
	}
});

document.title = "Inquiry Details";
</script>


