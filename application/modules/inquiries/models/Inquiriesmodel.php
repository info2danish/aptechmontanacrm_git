<?PHP
class Inquiriesmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	 {
	 	
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	 }
	 
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		//print_r($this->db->last_query());
		//exit;
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	 
	function getRecords($get){
		//echo "here...<br/>";
		//print_r($get);
		
		$table = "tbl_inquiry_master";
		$table_id = 'i.inquiry_master_id';
		$default_sort_column = 'i.inquiry_master_id';
		$default_sort_order = 'desc';
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('a.academic_year_master_name','z.zone_name','c.center_name','ct.categoy_name', 'cu.course_name','i.inquiry_no', 'i.student_first_name', 'i.student_last_name', 'i.inquiry_status','i.inquiry_progress');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<10;$i++)
		{
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $colArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "Condition: ".$condition;
		//exit;
		$this -> db -> select('i.*, a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');
		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('i.*, a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');
		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		
		$query1 = $this -> db -> get();
		//echo "total: ".$query1 -> num_rows();
		//exit;
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	}
	
	function getFormdata($ID){
		
		$this -> db -> select('i.*, a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name, co.country_name, s.state_name, cts.city_name');
		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		
		$this -> db -> join('tbl_countries as co', 'i.country_id  = co.country_id', 'left');
		$this -> db -> join('tbl_states as s', 'i.state_id  = s.state_id', 'left');
		$this -> db -> join('tbl_cities as cts', 'i.city_id  = cts.city_id', 'left');
		
		$this -> db -> where('i.inquiry_master_id', $ID);
	
	   $query = $this -> db -> get();
	   
	   //print_r($this->db->last_query());
	   //exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
		
	function checkRecord($tbl_name,$POST,$condition){
		
		//print_r($POST);
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	 function getExportRecords($get){
		$table = "tbl_feedbacks";
		$table_id = 'i.feedback_id';
		$default_sort_column = 'i.feedback_id';
		$default_sort_order = 'desc';
		$condition = "";
		$condition .= "  1=1 ";
		
		//$colArray = array('feedback_name','feedback_email','feedback_contact','created_on');
		$colArray = array('a.academic_year_master_name','z.zone_name','c.center_name','ct.categoy_name', 'cu.course_name','i.inquiry_no', 'i.student_first_name', 'i.student_last_name', 'i.inquiry_status','i.inquiry_progress');
		$searchArray = array('a.academic_year_master_name','z.zone_name','c.center_name','ct.categoy_name', 'cu.course_name','i.inquiry_no', 'i.student_first_name', 'i.student_last_name', 'i.inquiry_status','i.inquiry_progress');
		
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;
		
		for($i=0;$i<10;$i++){
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!=''){
				$condition .= " AND $colArray[$i] like '%".$get['sSearch_'.$i]."%'";
			}
		}
		
		$this -> db -> select('i.*, a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');
		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			$totcount = $query -> num_rows();
			return array("query_result" => $query->result(), "totalRecords" => $totcount);
		}else{
			return array("totalRecords" => 0);
		}
	}
}
?>