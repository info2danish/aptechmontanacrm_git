<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Usergroups extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('usergroupsmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
				if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['record_id'] = $_GET['text'];
			$this->load->view('template/header.php');
			$this->load->view('usergroups/index',$result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['id']) && isset($_GET['id'])) {
				$varr = base64_decode(strtr($_GET['id'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['record_id'] = $_GET['id'];
			$result['groups'] = $this->usergroupsmodel->getDropdown("tbl_group_master","group_master_id,group_master_name");
			
			$this->load->view('template/header.php');
			$this->load->view('usergroups/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	// // /*new code end*/
	function fetch($id=null)
	{
		$_GET['user_id'] = $id;

		$get_result = $this->usergroupsmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; 
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; 
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->first_name);
				array_push($temp, $get_result['query_result'][$i]->last_name);
				array_push($temp, $get_result['query_result'][$i]->group_master_name);
				
				$actionCol21="";
				// if($this->privilegeduser->hasPrivilege("")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->center_user_group_id. '\');" title="">Delete</a>';
				// }
				
				array_push($temp, $actionCol21);
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			if(empty($_POST['group_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select group!'));
				exit;
			}

			if(!empty($_POST['group_id'])){
				
				for($i=0; $i < sizeof($_POST['group_id']); $i++){
				    $condition = "user_id = ".$_POST['user_id']."  && group_id = ".$_POST['group_id'][$i] ;
				    $check_name = $this->usergroupsmodel->getdata('tbl_center_user_groups',$condition);
				    if(empty($check_name)){
    					$data_array = array();
    					$data_array['user_id'] = (!empty($_POST['user_id'])) ? $_POST['user_id'] : '';
    					$data_array['group_id'] = $_POST['group_id'][$i];
    					
    					$data_array['created_on'] = date("Y-m-d H:i:s");
						$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data_array['updated_on'] = date("Y-m-d H:i:s");
						$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;

    					$result = $this->usergroupsmodel->insertData('tbl_center_user_groups', $data_array, '1');
				    }
				    else{
				    	$getGroupName = $this->usergroupsmodel->getdata("tbl_group_master", "group_master_id='".$_POST['group_id'][$i]."' ");
						echo json_encode(array("success"=>"0",'msg'=>' '.$getGroupName[0]['group_master_name'].' Already assign to selected user!'));
						exit;
				    }
				}
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function delRecord()
	{
		$id=$_POST['id'];
		$appdResult = $this->usergroupsmodel->delrecord1("tbl_center_user_groups","center_user_group_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
