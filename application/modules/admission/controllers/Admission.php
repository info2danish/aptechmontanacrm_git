<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Admission extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('admissionmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('admission/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->admissionmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->student_first_name);
				array_push($temp, $get_result['query_result'][$i]->student_last_name);
				array_push($temp, $get_result['query_result'][$i]->enrollment_no);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);	
				
				$viewPaymentDetails="";
				// if($this->privilegeduser->hasPrivilege("")){
					$viewPaymentDetails.= '<a href="paymentdetails/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->admission_fees_id) , '+/', '-_') , '=') . '" title="Payment Details">View Payment Details</a>';
				// }
				array_push($temp, $viewPaymentDetails);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}

	function export(){
		$get_result = $this->admissionmodel->getExportRecords($_POST);
		
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0)
		{
			$this->load->library('Excel');
			
			//Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			//Set properties
			$objPHPExcel->getProperties()->setCreator("Attoinfotech")
			->setLastModifiedBy("Attoinfotech")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Export Excel");
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			
			// Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('OutPut-File');
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("A1", 'First Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("B1", 'Last Name', PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("C1", 'Enrollment No', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("D1", 'Zone', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("E1", 'Center', PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
			
			$j = 2;
			
			for($i=0;$i<sizeof($get_result['query_result']);$i++)
			{
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("A$j", $get_result['query_result'][$i]->student_first_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("B$j", $get_result['query_result'][$i]->student_last_name);
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("C$j", $get_result['query_result'][$i]->enrollment_no);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("D$j", $get_result['query_result'][$i]->zone_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("E$j", $get_result['query_result'][$i]->center_name);
				
				$j++;
			}
			
			// Redirect output to a client's web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Admission('.date('d-m-Y').').xls"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			
			$_SESSION['admission_export_success'] = "success";
			$_SESSION['admission_export_msg'] = "Records Exported Successfully.";

			redirect('admission');
			exit;
		}
		else
		{
			$_SESSION['contactus_export_success'] = "error";
			$_SESSION['contactus_export_msg'] = "No records found for export";	
			redirect('admission');
			exit;
		}
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->admissionmodel->delrecord12("tbl_student_master","student_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	
}

?>
