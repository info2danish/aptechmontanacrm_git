<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Camera extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('cameramodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('camera/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['details'] = $this->cameramodel->getFormdata($record_id);
			// print_r($result);
			// exit();
			$this->load->view('template/header.php');
			$this->load->view('camera/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
		
	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			$condition = "camera_name='".$_POST['camera_name']."' && url='".$_POST['url']."' &&  start_time='".$_POST['start_time']."' &&  end_time='".$_POST['end_time']."' ";

			if(strtotime($_POST['start_time']) > strtotime($_POST['end_time'])){
				echo json_encode(array("success"=>"0",'msg'=>'Start time not greater than end time!'));
				exit;
			}
			if(isset($_POST['camera_id']) && $_POST['camera_id'] > 0)
			{
				$condition .= " &&  camera_id != ".$_POST['camera_id'];
			}
			
			$check_name = $this->cameramodel->getdata("tbl_camera",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			
			if (!empty($_POST['camera_id'])) {
				$data_array = array();			
				$camera_id = $_POST['camera_id'];
		 		
				$data_array['camera_name'] = (!empty($_POST['camera_name'])) ? $_POST['camera_name'] : '';
				$data_array['url'] = (!empty($_POST['url'])) ? $_POST['url'] : '';
				$data_array['start_time'] = (!empty($_POST['start_time'])) ? date("G:i", strtotime($_POST['start_time'])) : '';
				$data_array['end_time'] = (!empty($_POST['end_time'])) ? date("G:i", strtotime($_POST['end_time']))  : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->cameramodel->updateRecord('tbl_camera', $data_array,'camera_id',$camera_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['camera_name'] = (!empty($_POST['camera_name'])) ? $_POST['camera_name'] : '';
				$data_array['url'] = (!empty($_POST['url'])) ? $_POST['url'] : '';
				$data_array['start_time'] = (!empty($_POST['start_time'])) ? date("G:i", strtotime($_POST['start_time'])) : '';
				$data_array['end_time'] = (!empty($_POST['end_time'])) ? date("G:i", strtotime($_POST['end_time']))  : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->cameramodel->insertData('tbl_camera', $data_array, '1');
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->cameramodel->getRecords($_GET);

		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->camera_name);
				array_push($temp, $get_result['query_result'][$i]->url);
				array_push($temp, date("h:i a", strtotime($get_result['query_result'][$i]->start_time)));
				array_push($temp, date("h:i a", strtotime($get_result['query_result'][$i]->end_time)));
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CameraAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->camera_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("CameraAddEdit")){
					$actionCol1.= '<a href="camera/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->camera_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord12()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->cameramodel->delrecord12("tbl_camera","camera_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
}

?>
