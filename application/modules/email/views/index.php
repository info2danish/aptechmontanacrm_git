<div id="content" class="content-wrapper">
   <div class="page-title">
      <div>
        <h1><i class=" "></i> Email Content</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><i class="icon-home"></i><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
		  <li><a href="<?php echo base_url()?>email">Email</a></li>
        </ul>
      </div>
    </div>  

    
	<div class="card">
		<div class="page-title-border">
            <div class="col-sm-12 col-md-6">
			<div class="box-content form-inline">
                <div class="dataTables_filter searchFilterClass form-group">
                    <label for="firstname" class="control-label">Subject</label>
                    <input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
                </div>
                <div class="control-group clearFilter form-group" style="margin-left:5px;">
                    <div class="controls">
                        <a href="#" onclick="clearSearchFilters();"><button class="btn btn-primary">Clear Search</button></a>
                    </div>
                </div>
                </div>

                </div>   
                <div class="col-sm-12 col-md-6 right-button-top">
                    <?php 
                        //if ($this->privilegeduser->hasPrivilege("AddEmailContent")) {
                    ?>
                        <p> <a href="<?php echo base_url();?>email/addEdit"><button class="btn">Add </button></a></p>
                    <?php //}?>
                    <div class="clearfix"></div>
                </div> 
                 
                	</div> 
                                               
        <div class="clearfix"></div>
        <div class="card-body">
        	<div class="box-content">
            	<div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap">
					<thead>
						<tr>
							<th>Title</th>
							<th>From</th>
							<th>Subject</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
					</tfoot>
				</table>
				</div>
		</div>
		<!--/span-->
	</div>
</div>
		</div>
<script>
	$( document ).ready(function() {

		clearSearchFilters();
	});
	function deleteData(id)
	{
    	var r=confirm("Are you sure you want to delete this record?");
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/emails/delRecord/"+id,
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Deleted!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Something Went Worng!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	document.title = "Emails Contents";
</script>
