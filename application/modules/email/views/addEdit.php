							<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
							<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
                            
                            
							<div id="content" class="content-wrapper">
                                <div class="page-title">
                                  <div>
                                    <h1><i class=" "></i> Add Email</h1>            
                                  </div>
                                  <div>
                                    <ul class="breadcrumb">
                                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                                      <li><a href="<?php echo base_url();?>email">Add Email</a></li>
                                    </ul>
                                  </div>
                                </div>                            
						
                                <div class="card">
                                    <div class="card-body">
                                        <div class="box-content">
                                            <form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                                    <fieldset>
                                        <input type="hidden" id="eid" name="eid" value="<?php if(!empty($details[0]->eid)){echo $details[0]->eid;}?>" />
                                        
                                        <div class="control-group">
                                            <label class="control-label"><span>Title</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required" placeholder="Enter title" id="title" name="title" value="<?php if(!empty($details[0]->title)){echo $details[0]->title;}?>" >
                                                <input type="hidden" id="eid" name="eid" value="<?php if(!empty($details[0]->eid)){echo $details[0]->eid;}?>" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span>From</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control" placeholder="Enter from name" id="fromname" name="fromname" value="<?php if(!empty($details[0]->fromname)){echo $details[0]->fromname;}?>">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span>From Email</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control email" placeholder="Enter email" id="fromemail" name="fromemail" value="<?php if(!empty($details[0]->fromemail)){echo $details[0]->fromemail;}?>">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span>Subject</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required" placeholder="Enter subject" id="subject" name="subject" value="<?php if(!empty($details[0]->subject)){echo $details[0]->subject;}?>">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span>Label</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required" placeholder="" id="label" name="label" value="<?php if(!empty($details[0]->label)){echo $details[0]->label;}?>">
                                            </div>
                                        </div> 
                                        <div class="control-group">
                                            <label class="control-label"><span>Content</span></label>
                                            <div class="controls">
                                                <textarea class="editor" id="content" name="content" rows="3"><?php if(!empty($details[0]->content)){echo $details[0]->content;}?></textarea>
                                            </div> 
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"><span>FCM Notification Text</span></label>
                                            <div class="controls">
                                                <textarea class="form-control required" id="fcm_notification" name="fcm_notification" rows="3"><?php if(!empty($details[0]->fcm_notification)){echo $details[0]->fcm_notification;}?></textarea>
                                            </div> 
                                        </div>
                                          <div class="clearfix" style="height: 10px; width: 100%; float: left; display: inline;">&nbsp;</div>
                                          
                                         <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="<?php echo base_url();?>email"><button class="btn" type="button">Cancel</button></a>
                                           
                                        </div>
                                      
                                    </fieldset>
                                  </form>  
                                        </div> 
                                    </div>
                                </div> 
							</div>
                              
<script>

$( document ).ready(function() {
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						['Source', 'Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
						['Find','Replace','-','Subscript','Superscript'],
						['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['BidiLtr', 'BidiRtl' ],
						['Maximize', 'ShowBlocks'],['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "100%"
			};
	$('.editor').ckeditor(config);
});

var vRules = {
	
	title:{required:true},
	fromname:{required:true},
	fromemail:{required:true},
	subject:{required:true},
	label:{required:true},
	content:{required:true},
	fcm_notification:{required:true},
};
var vMessages = {
	
	title:{required:"Please enter Title"},
	fromname:{required:"Please enter From name"},
	fromemail:{required:"Please enter From email"},
	subject:{required:"Please enter subject"},
	label:{required:"Please enter label"},
	content:{required:"Please enter content"},
	fcm_notification:{required:"Please enter content"},
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>email/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>email";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "Emails Contents";
</script>								
					
	
