<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Camerasettings extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('camerasettingsmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
		
			$result['zones'] = $this->camerasettingsmodel->getDropdown("tbl_zones","zone_id,zone_name");
		
			$this->load->view('template/header.php');
			$this->load->view('index',$result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->camerasettingsmodel->getFormdata($record_id);
			$result['zones'] = $this->camerasettingsmodel->getDropdown("tbl_zones","zone_id,zone_name");
            
			$this->load->view('template/header.php');
			$this->load->view('camerasettings/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code*/
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->camerasettingsmodel->getFormdata($record_id);
			
			if(!empty($result['details'])){
				//$result['productImages'] = $this->camerasettingsmodel->getProductImages($record_id);
			}
			
			$this->load->view('template/header.php');
			$this->load->view('camerasettings/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	
	/*new code end*/
	function fetch()
	{
		$get_result = $this->camerasettingsmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->max_connections);
				array_push($temp, $get_result['query_result'][$i]->max_viewing_timing);
				array_push($temp, $get_result['query_result'][$i]->cooling_period);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CenterCameraSettingsAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->camera_setting_id . '\',\'' . $get_result['query_result'][$i]->status. '\');" title="Delete">'.$get_result['query_result'][$i]->status .'</a>';
				}	
				
				
				$actionCol = "";
				if($this->privilegeduser->hasPrivilege("CenterCameraSettingsAddEdit")){
					$actionCol.= '<a href="camerasettings/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->camera_setting_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol);
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$product_id = "";
			
			/*if(isset($_POST["product_image"]) && !empty($_POST["product_image"])){
				unset($_POST["product_image"]);
			}
			*/
			
			$condition = "center_id='".$_POST['center_id']."' ";
			
			if(!empty($_POST['camera_setting_id'])){
				$condition .=" && camera_setting_id != '".$_POST['camera_setting_id']."' ";
			}
			$check_name = $this->camerasettingsmodel->getdata('tbl_camera_settings',$condition);
			/*echo "<pre>";
			echo $check_name[0]['camera_setting_id'];
			print_r($check_name);exit;
			exit;
			*/
			
			if(!empty($check_name[0]['camera_setting_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'Center Record Already Present!'));
				exit;
			}
			
			if (!empty($_POST['camera_setting_id'])) {
				$data_array=array();			
				$camera_setting_id = $_POST['camera_setting_id'];
		 		
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '0';
				
				$data_array['max_connections'] = (!empty($_POST['max_connections'])) ? $_POST['max_connections'] : '';
				$data_array['max_viewing_timing'] = (!empty($_POST['max_viewing_timing'])) ? $_POST['max_viewing_timing'] : '';
				$data_array['cooling_period'] = (!empty($_POST['cooling_period'])) ? $_POST['cooling_period'] : '';
				
				$data_array['updated_by']=$_SESSION["webadmin"][0]->user_id;
				
				$result = $this->camerasettingsmodel->updateRecord('tbl_camera_settings', $data_array,'camera_setting_id',$camera_setting_id);
				
		
			}else {
				
				$data_array=array();
				
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '0';
				
				$data_array['max_connections'] = (!empty($_POST['max_connections'])) ? $_POST['max_connections'] : '';
				$data_array['max_viewing_timing'] = (!empty($_POST['max_viewing_timing'])) ? $_POST['max_viewing_timing'] : '';
				$data_array['cooling_period'] = (!empty($_POST['cooling_period'])) ? $_POST['cooling_period'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->camerasettingsmodel->insertData('tbl_camera_settings', $data_array, '1');
				$camera_setting_id = $result;
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->camerasettingsmodel->delrecord("tbl_camera_settings","camera_setting_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->camerasettingsmodel->delrecord12("tbl_camera_settings","camera_setting_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	public function getCenters(){
		$result = $this->camerasettingsmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
