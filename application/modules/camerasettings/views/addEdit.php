<?php 
error_reporting(0);
$arrayYesNo = array('Yes'=>'Yes','No'=>'No');
$arrayActive = array('Active'=>'Active','Inactive'=>'Inactive','SoldOut'=>'Sold Out');

?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Center Camera Settings</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>camerasettings">Center Camera Settings</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="camera_setting_id" name="camera_setting_id" value="<?php if(!empty($details[0]->camera_setting_id)){echo $details[0]->camera_setting_id;}?>" />
							
								<div class="control-group form-group">
									<label class="control-label" for="zone_id">Zone*</label>
									<div class="controls">
										<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);" >
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
														$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center*</span></label> 
									<div class="controls">
										<select id="center_id" name="center_id" class="form-control" >
											<option value="">Select Center</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Max Connections*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Max Connections" id="max_connections" name="max_connections" value="<?php if(!empty($details[0]->max_connections)){echo $details[0]->max_connections;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Max Viewing Timing (In Minutes)*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Max Viewing Timming" id="max_viewing_timing" name="max_viewing_timing" value="<?php if(!empty($details[0]->max_viewing_timing)){echo $details[0]->max_viewing_timing;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Cooling Period (In Minutes)*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Cooling Period" id="cooling_period" name="cooling_period" value="<?php if(!empty($details[0]->cooling_period)){echo $details[0]->cooling_period;}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>camerasettings" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {

	$(".datepicker").datetimepicker({
		format: 'YYYY-MM-DD H:m:s'
	});
	
	<?php 
		if(!empty($details[0]->camera_setting_id)){
	?>
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						['Source', 'Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
						['Find','Replace','-','Subscript','Superscript'],
						['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['BidiLtr', 'BidiRtl' ],
						['Maximize', 'ShowBlocks'],['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>camerasettings/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	
	zone_id:{required:true},
	center_id:{required:true},
	max_connections:{required:true, digits:true},
	max_viewing_timing:{required:true, digits:true},
	cooling_period:{required:true, digits:true}
	
};
var vMessages = {
	
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	max_connections:{required:"Please enter max connections value."},
	max_viewing_timing:{required:"Please enter max viewing timing."},
	cooling_period:{required:"Please enter cooling period."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>camerasettings/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>camerasettings";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});



$(document).ready(function(){
		
 	
});


document.title = "AddEdit - Camera Settings";

 
</script>					
