<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Categories extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('categoriesmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->categoriesmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('categories/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code*/
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->categoriesmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('categories/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code end*/
	function fetch()
	{
		$get_result = $this->categoriesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CategoryAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->category_id . '\',\'' . $get_result['query_result'][$i]->status. '\');" title="Status">'.$get_result['query_result'][$i]->status .'</a>';
				}
				
				$actionCol = "";
				if($this->privilegeduser->hasPrivilege("CategoryAddEdit")){
					$actionCol.= '<a href="categories/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->category_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
				}	
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			$condition = "categoy_name='".$_POST['categoy_name']."' ";
			if(isset($_POST['category_id']) && $_POST['category_id'] > 0)
			{
				$condition .= " &&  category_id != ".$_POST['category_id'];
			}
			
			$check_name = $this->categoriesmodel->getdata("tbl_categories",$condition);
			//echo "<pre>";print_r($check_name);exit;
			//echo $_SESSION["webadmin"][0]->user_name;exit;
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			$product_id = "";
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["cover_image"]["name"]))
			{
				$config['upload_path'] = DOC_ROOT_FRONT."/images/category_images/";
				$config['max_size']    = '1000000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['file_name']     = md5(uniqid("100_ID", true));
				//$config['file_name']     = $_FILES["cover_image"]["name"].;
				
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("cover_image"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name'];
				}
				
				/* Unlink previous category image */
				if(!empty($_POST['category_id']))
				{
					$image = $this->categoriesmodel->getFormdata($_POST['category_id']);
					if(is_array($image) && !empty($image[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/category_images/".$image[0]->cover_image))
					{
						@unlink(DOC_ROOT_FRONT."/images/category_images/".$image[0]->cover_image);
					}
				}
				
			}
			else
			{
				$thumnail_value = $_POST['input_cover_image'];
			}
			
			if (!empty($_POST['category_id'])) {
				$data_array=array();			
				$category_id = $_POST['category_id'];
				$data_array['categoy_name'] = (!empty($_POST['categoy_name'])) ? $_POST['categoy_name'] : '';
				$data_array['category_description'] = (!empty($_POST['category_description'])) ? htmlentities($_POST['category_description']) : '';
				$data_array['cover_image'] = $thumnail_value;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by']=$_SESSION["webadmin"][0]->user_id;
				
				$result = $this->categoriesmodel->updateRecord('tbl_categories', $data_array,'category_id',$category_id);
				
			}else {
				$data_array=array();
				$data_array['categoy_name'] = (!empty($_POST['categoy_name'])) ? $_POST['categoy_name'] : '';
				$data_array['category_description'] = (!empty($_POST['category_description'])) ? htmlentities($_POST['category_description']) : '';
				$data_array['cover_image'] = $thumnail_value;
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->categoriesmodel->insertData('tbl_categories', $data_array, '1');
				$result = 1;
				//echo "result: ";$result;exit;
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && category_id='".$_GET['id']."' ";
		$result['details'] = $this->categoriesmodel->getdata('tbl_categories',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('categories/changeapproval',$result,true);
		echo $view;
	}
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->categoriesmodel->delrecord("tbl_categories","category_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->categoriesmodel->delrecord12("tbl_categories","category_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
