<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Category</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>categories">Category</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="category_id" name="category_id" value="<?php if(!empty($details[0]->category_id)){echo $details[0]->category_id;}?>" />
							
								
								<div class="control-group form-group">
									<label class="control-label"><span>Categoy Name*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Category Name" id="categoy_name" name="categoy_name" value="<?php if(!empty($details[0]->categoy_name)){echo $details[0]->categoy_name;}?>" >
									</div>
								</div>
								
								
								<div class="control-group form-group">
									<label class="control-label" for="cover_image">Image</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_cover_image" value="<?php if(!empty($details[0]->cover_image)){echo $details[0]->cover_image;}?>" name="input_cover_image" type="hidden" >
										
										<input class="input-xlarge" id="cover_image" name="cover_image" type="file">
										Only (gif, jpg, png, jpeg) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/category_images/".$details[0]->cover_image))
										{
										?>
											<img style="width: 150px;height:150px;padding-top:5px;" src="<?php echo FRONT_URL; ?>/images/category_images/<?php echo $details[0]->cover_image; ?>">
											</img>
											
										<?php 
										}
										?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Description</span></label>
									<br/>(Editor is for text contens only kindly don't put any image)
									<div class="controls">
										<textarea name="category_description" id="category_description" placeholder="Enter Description" class="form-control editor"><?php if(!empty($details[0]->category_description)){echo $details[0]->category_description;}?></textarea>
									</div>
								</div>
								
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>categories" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

var vRules = {
	
	categoy_name:{required:true, alphanumericwithspace:true},
	category_description:{required:true}
	
};
var vMessages = {
	
	categoy_name:{required:"Please enter category name."},
	category_description:{required:"Please enter description."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>categories/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>categories";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "AddEdit - Category";

 
</script>					
