<?php 
//session_start();

//print_r($_SESSION["webadmin"]);

//print_r($users);
//echo "Name: ".$users[0]->first_name;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Add Permission</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Add Permission</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
            	<div class="col-sm-8 col-md-4">
                <form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">                   
                    <input type="hidden" id="perm_id" name="perm_id" value="<?php if(!empty($users[0]->perm_id)){echo $users[0]->perm_id;}?>" />                    
                    <div class="control-group form-group">
                        <label class="control-label" for="perm_desc">Permission*</label>                            
                        <input class="input-xlarge form-control" id="perm_desc" name="perm_desc" type="text" value="<?php if(!empty($users[0]->perm_desc)){echo $users[0]->perm_desc;}?>">
                    </div>
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="<?php echo base_url();?>permission"><button class="btn" type="button">Cancel</button></a>
                    </div>                   
                </form>
                </div>
                <div class="clearfix"></div>
            </div>
         </div>              
    </div>
	</div><!-- end: Content -->
			
<script>

$( document ).ready(function() {
});

var vRules = {
	perm_desc:{required:true}
	
};
var vMessages = {
	perm_desc:{required:"Please enter permission."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>permission/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>permission";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "Add Edit Permission";

</script>