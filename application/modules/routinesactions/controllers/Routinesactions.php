<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Routinesactions extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('routinesactionsmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('routinesactions/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->routinesactionsmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('routinesactions/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$routine_action_id = "";
			
			$condition = "routine_type='".$_POST['routine_type']."' && routine_action= '".$_POST['routine_action']."' ";
			if(isset($_POST['routine_action_id']) && $_POST['routine_action_id'] > 0)
			{
				$condition .= " &&  routine_action_id != ".$_POST['routine_action_id'];
			}
			
			$check_name = $this->routinesactionsmodel->getdata("tbl_routine_actions_master",$condition);
			//echo "<pre>";print_r($check_name);exit;
			//echo $_SESSION["webadmin"][0]->user_name;exit;
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			//exit;
			if (!empty($_POST['routine_action_id'])) {
				$data_array=array();			
				$routine_action_id = $_POST['routine_action_id'];
		 		
				$data_array['routine_type'] = (!empty($_POST['routine_type'])) ? $_POST['routine_type'] : '';
				$data_array['routine_action'] = (!empty($_POST['routine_action'])) ? $_POST['routine_action'] : '';
				$data_array['updated_by']=$_SESSION["webadmin"][0]->user_id;
				
				$result = $this->routinesactionsmodel->updateRecord('tbl_routine_actions_master', $data_array,'routine_action_id',$routine_action_id);
				
			}else {
				
				$data_array = array();
				$data_array['routine_type'] = (!empty($_POST['routine_type'])) ? $_POST['routine_type'] : '';
				$data_array['routine_action'] = (!empty($_POST['routine_action'])) ? $_POST['routine_action'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->routinesactionsmodel->insertData('tbl_routine_actions_master', $data_array, '1');
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->routinesactionsmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->routine_type);
				array_push($temp, $get_result['query_result'][$i]->routine_action);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("RoutineActionAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->routine_action_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("RoutineActionAddEdit")){
					$actionCol1.= '<a href="routinesactions/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->routine_action_id) , '+/', '-_') , '=') . '" title="Edit">Edit</i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && routine_action_id='".$_GET['id']."' ";
		$result['details'] = $this->routinesactionsmodel->getdata('tbl_routine_actions_master',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->routinesactionsmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('routinesactions/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->routinesactionsmodel->delrecord("tbl_routine_actions_master","routine_action_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->routinesactionsmodel->delrecord12("tbl_routine_actions_master","routine_action_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	
	

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
