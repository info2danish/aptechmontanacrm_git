<?php 
error_reporting(0);
$arrayYesNo = array('Yes'=>'Yes','No'=>'No');
$arrayActive = array('Active'=>'Active','Inactive'=>'Inactive','SoldOut'=>'Sold Out');
//echo "<pre>";print_r($details[0]);exit;
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>CCTV Cumulative Settings</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>cumulativesettings">CCTV Cumulative Settings</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="admin_cumulative_setting_id" name="admin_cumulative_setting_id" value="<?php if(!empty($details[0]->admin_cumulative_setting_id)){echo $details[0]->admin_cumulative_setting_id;}?>" />
							
								<div class="control-group form-group">
									<label class="control-label" for="zone_id">Zone*</label>
									<div class="controls">
										<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);" >
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
														$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center*</span></label> 
									<div class="controls">
										<select id="center_id" name="center_id" class="form-control" >
											<option value="">Select Center</option>
										</select>
									</div>
								</div>	
								
								<?php 
									if(!empty($details[0]->admin_cumulative_setting_id)){
								?>
								
									<div class="control-group form-group">
										<label class="control-label"><span>Category_id*</span></label>
										<div class="controls">
											<select name="category_id" id="category_id_0" class="required form-control" title="Please select category." onchange="getCourseList(this.value,0)" >
												<option value="">Select Option</option>
												<?php 
													if(isset($categories) && !empty($categories)){
														foreach($categories as $cdrow){
															$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
												<?php }}?>
											</select>
										</div>
									</div>
									
									<div class="control-group form-group">
										<label class="control-label"><span>Course Name*</span></label>
										<div class="controls">
											<select name="course_id" id="course_id_0" class="required form-control" title="Please select course name.">
												<option value="">Select Option</option>
											</select>
										</div>
									</div>
									
									<div class="control-group form-group">
										<label class="control-label"><span>Max Viewing Time(Min)*</span></label>
										<div class="controls">
											<input type="text" name="cumulative_max_viewing_time" id="cumulative_max_viewing_time_0" class="required form-control number" title="Please enter valid max viewing time." placeholder="Please enter max viewing time."  onfocusout="getCumulativeTime(0)" value="<?php if(!empty($details[0]->cumulative_max_viewing_time)){echo $details[0]->cumulative_max_viewing_time;}?>" />
										</div>
									</div>
									
									<div class="control-group form-group">
										<label class="control-label"><span>Cumulative Time(Min)*</span></label>
										<div class="controls">
											<input type="text" name="admin_cumulative_time" id="admin_cumulative_time_0" class="required form-control number" title="Please enter valid cumulative time." placeholder="Please enter cumulative time." readonly value="<?php if(!empty($details[0]->admin_cumulative_time)){echo $details[0]->admin_cumulative_time;}?>" />
										</div>
									</div>
									
									<div class="control-group form-group">
										<label class="control-label"><span>Center Cumulative Time(Min)*</span></label>
										<div class="controls">
											<input type="text" name="center_cumulative_time" id="center_cumulative_time_0" class=" form-control number" title="Please enter valid cumulative time." placeholder="Please enter center cumulative time." value="<?php if(!empty($details[0]->center_cumulative_time)){echo $details[0]->center_cumulative_time;}?>" onfocusout="getCheckCumulativeTime();"  />
										</div>
									</div>
								
								<?php }else{?>
									
									<div class="col-sm-12 col-xs-12">
										<a class="minia-icon-file-add btn btn-primary tip" style="float: none; margin-right: 0%; margin-top: 3%; margin-bottom: 2%;" onclick="addFiles();" oldtitle="Add Co-Organizer" title="" aria-describedby="ui-tooltip-1"> Add Setting </a>
										<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
											<table class="display table table-bordered non-bootstrap">
												<thead>
													<tr>
														<th>Category*</th>
														<th>Course Name*</th>
														<th>Max Viewing Time(Min)*</th>
														<th>Cumulative Time(Min)*</th>
														<th>Action</th>
													</tr>
												</thead>
												
												<tbody id="tableBody">
												</tbody>
											</table>					
										</div>
									</div>
									<div style="clear:both;margin-bottom:20px;"></div>
									
								<?php }?>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>cumulativesettings" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

function getCheckCumulativeTime(){
	//alert("here");
	var admin_cumulative_time = $("#admin_cumulative_time_0").val();
	if(admin_cumulative_time == ""){
		alert("Please enter cumulative time.");
		return false;
	}
	
	var center_cumulative_time = $("#center_cumulative_time_0").val();
	//alert(" center_cumulative_time: "+center_cumulative_time+" admin_cumulative_time: "+admin_cumulative_time);
	//return false;
	center_cumulative_time = parseInt(center_cumulative_time);
	admin_cumulative_time = parseInt(admin_cumulative_time);
	if(center_cumulative_time > admin_cumulative_time){
		alert("Center cumulative time should less than or equal to cumulative time.");
		$("#center_cumulative_time_0").val("");
		return false;
	}
}

 var count = 1;
function addFiles()
{
	var fileCount = count - 1;
	var text = '<tr id="divTD_'+count+'">'+
					'<td>'+
						'<select name="category_id[]" id="category_id_'+count+'" class="required form-control" title="Please select course family." onchange="getCourseList(this.value,'+count+')" >'+
						'<option value="">Select Option</option>'+
						<?php 
							if(isset($categories) && !empty($categories)){
								foreach($categories as $cdrow){
						?>
							'<option value="<?php echo $cdrow->category_id;?>"><?php echo $cdrow->categoy_name;?></option>'+
						<?php }}?>
						'</select>'+
					'</td>'+
					'<td>'+
						'<select name="course_id[]" id="course_id_'+count+'" class="required form-control" title="Please select course name." >'+
						'<option value="">Select Option</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<input type="text" name="cumulative_max_viewing_time[]" id="cumulative_max_viewing_time_'+count+'" class="required form-control number" title="Please enter valid max viewing time." placeholder="Please enter max viewing time."  onfocusout="getCumulativeTime('+count+')" />'+
					'</td>'+
					'<td>'+
						'<input type="text" name="admin_cumulative_time[]" id="admin_cumulative_time_'+count+'" class="required form-control number" title="Please enter valid cumulative time." placeholder="Please enter cumulative time." readonly />'+
					'</td>'+
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles(\'divTD_'+count+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody").append(text);
	//$("#calling_day_"+count).select2();
	
	$(".datepicker").datetimepicker({
		//format: 'YYYY-MM-DD H:m:s'
		format: 'H:mm'
	});
	
	count++;
}

function removeFiles(divId,record_id)
{
    //alert(record_id);
	//return false;
	var r=confirm("Are you sure you want to delete this record?");
	if (r==true)
   	{
		if(record_id == '0'){
			//alert("in");
			$("#"+divId).remove();
		}else{
			$("#"+divId).remove();
		}
	}
}

function getCumulativeTime(count){
	var max_viewing_time = $("#cumulative_max_viewing_time_"+count).val();
	if(max_viewing_time == ""){
		alert("Please enter max viewing time.");
		return false;
	}
	
	if(max_viewing_time < 1){
		alert("Please enter valid max viewing time.");
		return false;
	}
	var cumulative_time = 0;
	cumulative_time = (max_viewing_time / 6);
	//alert(cumulative_time);
	cumulative_time = cumulative_time.toFixed();
	$("#admin_cumulative_time_"+count).val(cumulative_time);
	
}
 
$( document ).ready(function() {

	$(".datepicker").datetimepicker({
		//format: 'YYYY-MM-DD H:m:s'
		format: 'H:m'
	});
	
	<?php 
		if(!empty($details[0]->admin_cumulative_setting_id)){
	?>
		//alert("here..");return false;
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
		getCourseList('<?php echo $details[0]->category_id; ?>',0, '<?php echo $details[0]->course_id; ?>');
	<?php }else{?>
		addFiles();
	<?php }?>

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						['Source', 'Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
						['Find','Replace','-','Subscript','Superscript'],
						['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['BidiLtr', 'BidiRtl' ],
						['Maximize', 'ShowBlocks'],['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});



function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+zone_id);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>cumulativesettings/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCourseList(category_id,count,course_id = null)
{
	//alert("course_family_id: "+course_family_id);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>cumulativesettings/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id_"+count).html("<option value=''>Select</option>"+res['option']);
						//$("#region_text").select2();
					}
					else
					{
						$("#course_id_"+count).html("<option value=''>Select</option>");
						//$("#region_text").select2();
					}
				}
				else
				{	
					$("#course_id_"+count).html("<option value=''>Select</option>");
					//$("#region_text").select2();
				}
			}
		});
	}
}


var vRules = {
	
	zone_text:{required:true},
	region_text:{required:true},
	area_text:{required:true},
	center_id:{required:true}
	
};
var vMessages = {
	
	zone_text:{required:"Please select zone."},
	region_text:{required:"Please select region."},
	area_text:{required:"Please select area."},
	center_id:{required:"Please select center."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>cumulativesettings/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>cumulativesettings";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});



$(document).ready(function(){
		
 	
});


document.title = "AddEdit - CCTV Cumulative Settings";

 
</script>					
