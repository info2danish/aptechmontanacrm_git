<?PHP
class Usersmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	 {
	 	
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	 	
	 	
	 	
	 }
	 
	function getRecords($get){
		//echo "here...<br/>";
		//print_r($get);
		
		$table = "tbl_admin_users";
		$table_id = 'user_id';
		$default_sort_column = 'user_id';
		$default_sort_order = 'asc';
		$condition = " i.user_type=1 ";
		
		$colArray = array('i.role_id','i.user_name','i.first_name','i.last_name','i.phone');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<7;$i++)
		{
			if($i==8){
				if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
				{
					$condition .= " && $colArray[$i] = '".$get['sSearch_'.$i]."'";
				}
			}else{
				if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
				{
					$condition .= " && $colArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
				}
			}
		}
		
		//echo "Condition: ".$condition;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from('tbl_admin_users as i');
		$this -> db -> join('roles as r', 'i.role_id  = r.role_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('*');
		$this -> db -> from('tbl_admin_users as i');
		$this -> db -> join('roles as r', 'i.role_id  = r.role_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		
		$query1 = $this -> db -> get();
		//echo "total: ".$query1 -> num_rows();
		//exit;
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	}
	
	function getDropdown($tbl_name,$tble_flieds){
	   
	   $this -> db -> select($tble_flieds);
	   $this -> db -> from($tbl_name);
	
	   $query = $this -> db -> get();
	
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
			
	}
	
	function getDropdownSelval($tbl_name,$tbl_id,$tble_flieds,$rec_id=NULL){
		
	   $this -> db -> select($tble_flieds);
	   $this -> db -> from($tbl_name);
	   $this -> db -> where($tbl_id, $rec_id);
	
	   $query = $this -> db -> get();
		
	   //print_r($this->db->last_query());
	   //exit;
	   
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	function getFormdata($ID){
		
	   $this -> db -> select('u.*');
	   $this -> db -> from('tbl_admin_users as u');
	   $this -> db -> where('u.user_id', $ID);
	
	   $query = $this -> db -> get();
	   
	   //print_r($this->db->last_query());
	   //exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	
	
	//Update customer by id
	 function updateUserId($datar,$eid)
	 {
		 $this -> db -> where('user_id', $eid);
		 $this -> db -> update('tbl_admin_users',$datar);
		 
		 if ($this->db->affected_rows() > 0)
			{
			  return true;
			}
		 else
			{
			  return true;
			} 
		 
	 }
	 
	function delrecord($tbl_name,$tbl_id,$record_id,$data)
	{
		//print_r($data);exit;
		$this -> db -> where($tbl_id, $record_id);
		$this -> db -> update($tbl_name,$data);
		 
		if ($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return true;
		} 
		
		
	}
	
	function delrecords($tbl_name,$tbl_id,$record_id)
	 {
		 $this->db->where($tbl_id, $record_id);
	     $this->db->delete($tbl_name); 
		 if($this->db->affected_rows() >= 1)
	   {
	     return true;
	   }
	   else
	   {
	     return false;
	   }
	 }
	 
	function checkRecord($POST,$condition){
		
		//print_r($POST);
		//exit;
	   $this -> db -> select('r.user_id');
	   $this -> db -> from('tbl_admin_users as r');
	   $this->db->where("($condition)");
	
	   $query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
	   //exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	
	function checkRecord1($POST,$condition){
		
		//print_r($POST);
		//exit;
	   $this -> db -> select('r.user_id');
	   $this -> db -> from('tbl_admin_users as r');
	   $this->db->where("($condition)");
	
	   $query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
	   //exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	
	function delrecord_condition($tbl_name, $condition)
	{
		//$this->db->where($tbl_id, $record_id);
		$this -> db -> where("($condition)");
	    $this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	
	
 

}
?>