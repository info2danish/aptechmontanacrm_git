<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<link href="<?PHP echo base_url();?>css/jquery.fancybox.min.css" rel="stylesheet">
<script type="text/javascript" src="<?PHP echo base_url();?>js/jquery.fancybox.min.js"></script>

<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Timetable Details</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>centertimetables">Timetable Details</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="timetable_id" name="timetable_id" value="<?php if(!empty($details[0]->timetable_id)){echo $details[0]->timetable_id;}?>" />
								
								<div class="control-group form-group">
									<label class="control-label"><span>Timetable Title*</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->timetable_title)){echo $details[0]->timetable_title;}?>
									</div>
								</div>
								
								<!--<div class="control-group form-group">
									<label class="control-label"><span>Timetable Name*</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->timetable_name)){echo $details[0]->timetable_name;}?>
									</div>
								</div>-->
								
								<div class="control-group form-group">
									<label class="control-label" for="cover_image">Cover Image*</label>
									<div class="controls">
										<?php if(!empty($details[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/timetable_images/".$details[0]->cover_image))
										{
										?>
											<img style="width: 50;height:50;padding-top:5px;" src="<?php echo FRONT_URL; ?>/images/timetable_images/<?php echo $details[0]->cover_image; ?>">
											</img>
											
										<?php 
										}
										?>
									</div>
								</div>
								<h2><strong>Timetable Videos</strong></h2>
								<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
									<table class="display table table-bordered non-bootstrap">
										<thead>
											<tr>
												<th>Video Title</th>
												<th>Video URL</th>
											</tr>
										</thead>
										
										<tbody id="tableBody">
											<?php
												if(!empty($videos)){
													$count = 1;
													for($i=0;$i<sizeof($videos);$i++){
												?>	
													<tr id="divTD_<?php echo $count;?>">
														<td>
															<?php if(!empty($videos[$i]['video_title'])){ echo $videos[$i]['video_title']; }?>
														</td>
														<td>
															<iframe src="<?php echo $videos[$i]['video_url']; ?>" style="border:0;height:200;width:350px;max-width: 100%" allowFullScreen="true" allow="encrypted-media"></iframe>
															
														</td>
													</tr>
												<?php $count++; }}?>
										</tbody>
									</table>					
								</div>
								<div style="clear:both; margin-bottom: 2%;"></div>
								<h2><strong>Timetable Documents</strong></h2>
								<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
									<table class="display table table-bordered non-bootstrap">
										<thead>
											<tr>
												<th>Document Title</th>
												<th>Description</th>
												<th>Document</th>
											</tr>
										</thead>
										
										<tbody id="tableBody1">
											<?php
												if(!empty($documents)){
													$count1 = 1;
													for($i=0;$i<sizeof($documents);$i++){
												?>	
													<tr id="div1TD_<?php echo $count1;?>">
														<td>
															<?php if(!empty($documents[$i]['doc_title'])){ echo $documents[$i]['doc_title']; }?>
														</td>
														<td>
															<?php if(!empty($documents[$i]['doc_description'])){ echo $documents[$i]['doc_description']; }?>
														</td>
														<td>
															<!--<a href="javascript:void(0);" onclick="openwindow('<?php echo $documents[$i]['doc_file']; ?>');" title="">View Document</a>-->
															<?php if(!empty($documents[$i]['doc_type']) && $documents[$i]['doc_type'] == 'image'){?>
																<a class="fancybox"  href="<?php echo FRONT_URL; ?>/images/timetable_images/<?php echo $documents[$i]['doc_file']; ?> "  title="">View Document</a>
															<?php }else{?>
																<a href="<?php echo base_url();?>centertimetables/viewDoc?text=<?php echo $documents[$i]['timetable_document_id']; ?>"  title="">View Document</a>
															<?php }?>
														</td>
													</tr>
												<?php $count1++; }}?>
										</tbody>
									</table>					
								</div>
								<div style="clear:both; margin-bottom: 2%;"></div>
								<div class="form-actions form-group">
									<a href="<?php echo base_url();?>centertimetables" class="btn btn-primary">Back</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

function openwindow(imgname){
	//alert("here...");
	window.open("<?php echo FRONT_URL; ?>/images/timetable_images/"+imgname, "Document", "width=500,height=500");
}
 
$( document ).ready(function() {
	$("body").on("contextmenu",function(e){
        return false;
	});
	
	$(".fancybox").fancybox();
	
	//$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	
	$('.iframe').bind("contextmenu", function(e) {
	    console.log(e);
	    return false;
	});
	
	<?php 
		if(!empty($details[0]->timetable_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getThemes('<?php echo $details[0]->course_id; ?>', '<?php echo $details[0]->theme_id; ?>');
		getWeeks('<?php echo $details[0]->theme_id; ?>', '<?php echo $details[0]->week_id; ?>');
		count = '<?php echo $count;?>';
		count1 = '<?php echo $count1;?>';
	<?php }else{?>
		addFiles();
		addFiles1();
	<?php }?>
});

var count = 1;
function addFiles()
{
	var fileCount = count - 1;
	var text = '<tr id="divTD_'+count+'">'+
					'<td>'+
						'<input type="text" name="video_title[]" id="video_title_'+count+'" class="required form-control" title="Please enter title." />'+
					'</td>'+
					'<td>'+
						'<textarea name="video_url[]" id="video_url_'+count+'" class="required form-control" title="Please enter video url." ></textarea>'+
					'</td>'+
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles(\'divTD_'+count+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody").append(text);
	count++;
}

function removeFiles(divId,record_id)
{
    //alert(record_id);
	//return false;
	var r=confirm("Are you sure you want to delete this record?");
	if (r==true)
   	{
		if(record_id == '0'){
			//alert("in");
			$("#"+divId).remove();
		}else{
			//alert("else");
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/timetablemaster/delRecord/",
				data:{"id":record_id},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Deleted!");
						//setTimeout("location.reload(true);",1000);
						$("#"+divId).remove();
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						//setTimeout("location.reload(true);",1000);
					}
				}
			});
		}
	}
}


var count1 = 1;
function addFiles1()
{
	var fileCount = count1 - 1;
	var text = '<tr id="div1TD_'+count+'">'+
					'<td>'+
						'<input type="text" name="doc_title['+count1+']" id="doc_title_'+count1+'" value="" class="required form-control" title="Please enter title." />'+
						'<input type="hidden" name="timetable_document_id['+count1+']" id="timetable_document_id_'+count1+'" value="" />'+
					'</td>'+
					'<td>'+
						'<select name="doc_type['+count1+']" id="doc_type_'+count1+'" class="required form-control">'+
							'<option value=""> Select Type</option>'+
							'<option value="doc"> Document</option>'+
							'<option value="image"> Image</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<textarea name="doc_description['+count1+']" id="doc_description_'+count1+'" class="required form-control" title="Please enter description." ></textarea>'+
					'</td>'+
					'<td>'+
						'<input type="file" name="doc_file['+count1+']" id="doc_file_'+count1+'" />'+
					'</td>'+
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles1(\'div1TD_'+count+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody1").append(text);
	count1++;
}

function removeFiles1(divId,record_id)
{
    //alert(record_id);
	//return false;
	var r=confirm("Are you sure you want to delete this record?");
	if (r==true)
   	{
		if(record_id == '0'){
			//alert("in");
			$("#"+divId).remove();
		}else{
			//alert("else");
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/timetablemaster/delDocRecord/",
				data:{"id":record_id},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Deleted!");
						//setTimeout("location.reload(true);",1000);
						$("#"+divId).remove();
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						//setTimeout("location.reload(true);",1000);
					}
				}
			});
		}
	}
}


function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>timetablemaster/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getThemes(course_id,theme_id = null)
{
	//alert("Val: "+val);return false;
	if(course_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>timetablemaster/getThemes",
			data:{course_id:course_id, theme_id:theme_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#theme_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#theme_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#theme_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}


function getWeeks(theme_id,week_id = null)
{
	//alert("Val: "+val);return false;
	if(theme_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>timetablemaster/getWeeks",
			data:{theme_id:theme_id, week_id:week_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#week_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#week_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#week_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	course_id:{required:true},
	theme_id:{required:true},
	week_id:{required:true},
	timetable_title:{required:true, alphanumericwithspace:true},
	timetable_name:{required:true, alphanumericwithspace:true}
	
};
var vMessages = {
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	theme_id:{required:"Please select theme."},
	week_id:{required:"Please select week."},
	timetable_title:{required:"Please enter timetable title."},
	timetable_name:{required:"Please enter timetable name."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>timetablemaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>timetablemaster";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Timetable";

 
</script>					
