<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Roles extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('rolesmodel','',TRUE); 
   if(!$this->privilegeduser->hasPrivilege("RoleList")){
     redirect('home');
   }
 }
 
 function index()
 {
   if(!empty($_SESSION["webadmin"]))
   {
		$this->load->view('template/header.php');
		$this->load->view('roles/index');
		$this->load->view('template/footer.php');
   }
   else
   {
		//If no session, redirect to login page
		redirect('login', 'refresh');
   }
 }
 
 function fetch(){
	//print_r($_GET);
	$get_result = $this->rolesmodel->getRecords($_GET);
	
	//print_r($get_result['query_result']);
	//echo "Count: ".$get_result['totalRecords'];
	
	$result = array();
	$result["sEcho"]= $_GET['sEcho'];
	
	$result["iTotalRecords"] = $get_result['totalRecords'];									//	iTotalRecords get no of total recors
	$result["iTotalDisplayRecords"]= $get_result['totalRecords'];							//  iTotalDisplayRecords for display the no of records in data table.
	
	$items = array();
	if(!empty($get_result['query_result'])){
		for($i=0;$i<sizeof($get_result['query_result']);$i++){
			$temp = array();
			array_push($temp, $get_result['query_result'][$i]->role_name);
			
			$actionCol = "";
			if ($this->privilegeduser->hasPrivilege("RoleAddEdit")) {
				$actionCol .='<a href="roles/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->role_id), '+/', '-_'), '=').'" title="Edit"><i class="fa fa-edit"></i></a>';
			}
			if ($this->privilegeduser->hasPrivilege("RoleDelete")) {
	//			$actionCol .='&nbsp;&nbsp;<a href="javascript:void(0);" onclick="deleteData(\''.$get_result['query_result'][$i]->role_id .'\');" title="Delete"><i class="fa fa-remove"></i></a>';
			}
			
			array_push($temp, $actionCol);
			array_push($items, $temp);
		}
	}	
	
	$result["aaData"] = $items;
	echo json_encode($result);
	exit;
	
 }
 
function addEdit($id=NULL)
 {
   if(!empty($_SESSION["webadmin"]))
	{
     
		$record_id = "";
		//print_r($_GET);
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$record_id = $url_prams['id'];
		}
		
		//echo $user_id;
		$result = "";
		$result['users'] = $this->rolesmodel->getFormdata($record_id);
		$result['permissions'] = $this->rolesmodel->getPermdata();
		$result['selpermissions'] = $this->rolesmodel->getSelPermdata($record_id);
		
		//print_r($result);
		
		$this->load->view('template/header.php');
		$this->load->view('roles/addEdit',$result);
		$this->load->view('template/footer.php');
	}
	else
	{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
 }
 
	function submitForm(){
		/*print_r($_POST);
		exit;*/
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			
			if(!empty($_POST['role_id'])){
				//update
				/*echo "in update";
				print_r($_POST);
				exit;*/
				
				$data = array();
				$data['role_name'] = $_POST['role_name'];				
				//$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->rolesmodel->updateRecord($data,$_POST['role_id']);
				
				if(!empty($_POST['perm_id']) && isset($_POST['perm_id'])){
					$this->rolesmodel->delrecord("role_perm","role_id",$_POST['role_id']);
					for($i=0;$i<sizeof($_POST['perm_id']);$i++){
						$perm_data = array();
						$perm_data['role_id']= $_POST['role_id'];
						$perm_data['perm_id']= $_POST['perm_id'][$i];
						$rs = $this->rolesmodel->insertData('role_perm',$perm_data,'1');
					}
				}
					
				if(!empty($result)){
					echo json_encode(array('success'=>'1','msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>'0','msg'=>'Problem in data update.'));
					exit;
				}
				
				
			}else{
				//add
				/*echo "in add";
				print_r($_POST);
				exit;*/
				
				//print_r($_SESSION["webadmin"]);
				//echo $_SESSION["webadmin"][0]->user_id;
				//exit;
				
				$data = array();
				$data['role_name'] = $_POST['role_name'];
				//$data['created_on'] = date("Y-m-d H:i:s");
				//$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
				//$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->rolesmodel->insertData('roles',$data,'1');
				
				if(!empty($_POST['perm_id']) && isset($_POST['perm_id'])){
					for($i=0;$i<sizeof($_POST['perm_id']);$i++){
						$perm_data = array();
						$perm_data['role_id']= $result;
						$perm_data['perm_id']= $_POST['perm_id'][$i];
						$rs = $this->rolesmodel->insertData('role_perm',$perm_data,'1');
					}
				}
				
				if(!empty($result)){
					echo json_encode(array("success"=>"1",'msg'=>'Record inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array("success"=>"0",'msg'=>'Problem in data insert.'));
					exit;
				}
				
			}
			 
			
		}else{
			return false;
		}
	}
 
//For Delete

function delRecord($id)
 {
	 $appdResult = $this->rolesmodel->delrecord("roles","role_id",$id);
	 $appdResult1 = $this->rolesmodel->delrecord("role_perm","role_id",$id);
	 
	if($appdResult)
	{
		echo "1";
	}
	else
	{
		echo "2";	
			 
	}	
 }	
	
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('auth/login', 'refresh');
 }

}

?>