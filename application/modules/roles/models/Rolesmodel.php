<?PHP
class rolesmodel extends CI_Model
{ 
	function insertData($tbl_name,$data_array,$sendid = NULL)
	 {
	 	
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	 	
	 	
	 	
	 }
	 
	function getRecords($get){
		//echo "here...<br/>";
		//print_r($get);
		
		$table = "roles";
		$table_id = 'role_id';
		//$default_sort_column = 'perm_id';
		$default_sort_column = 'role_name';
		$default_sort_order = 'asc';
		$condition = "1=1";
		
		$colArray = array('i.role_name');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<7;$i++)
		{
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $colArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "Condition: ".$condition;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from('roles as i');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('*');
		$this -> db -> from('roles as i');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		
		$query1 = $this -> db -> get();
		//echo "total: ".$query1 -> num_rows();
		//exit;
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	}
	
	function getDropdown($tbl_name,$tble_flieds){
	   
	   $this -> db -> select($tble_flieds);
	   $this -> db -> from($tbl_name);
	
	   $query = $this -> db -> get();
	
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
			
	}
	
	function getFormdata($ID){
		
	   $this -> db -> select('i.*');
	   $this -> db -> from('roles as i');
	   $this -> db -> where('i.role_id', $ID);
	
	   $query = $this -> db -> get();
	   
	   //print_r($this->db->last_query());
	   //exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	
	function getPermdata(){
		
	   $this -> db -> select('i.*');
	   $this -> db -> from('permissions as i');
	   //$this -> db -> where('i.perm_id', $ID);
	   $this->db->order_by('i.perm_desc', 'asc');
	
	   $query = $this -> db -> get();
	   
	   //print_r($this->db->last_query());
	   //exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	
	function getSelPermdata($ID){
		
		$this -> db -> select('i.perm_id');
		$this -> db -> from('role_perm as i');
		$this -> db -> where('i.role_id', $ID);
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function updateRecord($datar,$eid)
	{
		$this -> db -> where('role_id', $eid);
		$this -> db -> update('roles',$datar);
		 
		if ($this->db->affected_rows() > 0)
			{
			  return true;
			}
		else
			{
			  return true;
			} 
		 
	}
	 
	function delrecord($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	 
	 
	
	
 

}
?>