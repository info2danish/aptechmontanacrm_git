<?php 
//error_reporting(0);

$selcourses1 = array();
if(!empty($selcourses)){
	for($i=0; $i < sizeof($selcourses); $i++){
		$selcourses1[] = $selcourses[$i]->course_id;
	}
}
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Document Folder</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>documentfoldermaster">Document Folder</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="document_folder_id" name="document_folder_id" value="<?php if(!empty($details[0]->document_folder_id)){echo $details[0]->document_folder_id;}?>" />
							<!--	
								<div class="control-group form-group">
									<label class="control-label" for="category_id">Category*</label>
									<div class="controls">
										<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
											<option value="">Select Category</option>
											<?php 
												if(isset($categories) && !empty($categories)){
													foreach($categories as $cdrow){
														$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="course_id">Courses*</label>
									<div class="controls">
										<select id="course_id" name="course_id[]" class="form-control select2" multiple >
											<option value="">Select Courses</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="zone_id">Zone*</label>
									<div class="controls">
										<select id="zone_id" name="zone_id" class="form-control" onchange="getCenters(this.value);"  >
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
														$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="center_id">Centers*</label>
									<div class="controls">
										<select id="center_id" name="center_id[]" class="form-control select2" multiple >
											<option value="">Select Center</option>
										</select>
									</div>
								</div>
							-->	
								<div class="control-group form-group">
									<label class="control-label"><span>Folder Name*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Folder Name" id="folder_name" name="folder_name" value="<?php if(!empty($details[0]->folder_name)){echo $details[0]->folder_name;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="cover_image">Cover Image</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_cover_image" value="<?php if(!empty($details[0]->cover_image)){echo $details[0]->cover_image;}?>" name="input_cover_image" type="hidden" >
										
										<input class="input-xlarge" id="cover_image" name="cover_image" type="file">
										Only (gif, jpg, png, jpeg) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/document_folder_images/".$details[0]->cover_image))
										{
										?>
											<img style="width: 150px;height:150px;padding-top:5px;" src="<?php echo FRONT_URL; ?>/images/document_folder_images/<?php echo $details[0]->cover_image; ?>">
											</img>
											
										<?php 
										}
										?>
									</div>
								</div>
								
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>documentfoldermaster" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>
$( document ).ready(function() {
	
	<?php 
		if(!empty($details[0]->document_folder_id)){
	?>
		//getCourses('<?php echo $details[0]->category_id; ?>');
		//getCenters('<?php echo $details[0]->zone_id; ?>');
	<?php }?>
	
	
});

function getCourses(category_id)
{
	//alert("center_id: "+center_id);return false;
	if(category_id != "" )
	{
		<?php 
			if(!empty($details[0]->document_folder_id)){
		?>
			var document_folder_id = '<?php echo $details[0]->document_folder_id; ?>';
		<?php }else{?>
			var document_folder_id = '';
		<?php }?>
		
		//alert();
		$.ajax({
			url:"<?php echo base_url();?>documentfoldermaster/getCourses",
			data:{category_id:category_id, document_folder_id:document_folder_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
						$("#course_id").select2();
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
						$("#course_id").select2();
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
					$("#course_id").select2();
				}
			}
		});
	}
}

function getCenters(zone_id)
{
	//alert("center_id: "+center_id);return false;
	if(zone_id != "" )
	{
		<?php 
			if(!empty($details[0]->document_folder_id)){
		?>
			var document_folder_id = '<?php echo $details[0]->document_folder_id; ?>';
		<?php }else{?>
			var document_folder_id = '';
		<?php }?>
		
		//alert();
		$.ajax({
			url:"<?php echo base_url();?>documentfoldermaster/getCenters",
			data:{zone_id:zone_id, document_folder_id:document_folder_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
						$("#center_id").select2();
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
						$("#center_id").select2();
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
					$("#center_id").select2();
				}
			}
		});
	}
}

var vRules = {
	//zone_id:{required:true},
	//category_id:{required:true},
	folder_name:{required:true, alphanumericwithspace:true}
	
};
var vMessages = {
	//zone_id:{required:"Please select zone."},
	//category_id:{required:"Please select category."},
	folder_name:{required:"Please enter folder name."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>documentfoldermaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>documentfoldermaster";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Document Folder";

 
</script>					
