<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo $tot_profile_percentage;exit;
?>
<script src="js/jquery.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="css/jquery-jvectormap.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>-->

<script type="text/javascript" src="js/loader.js"></script>

<!-- SlimScroll -->
<script src="js/jquery.slimscroll.min.js"></script>

<!-- jvectormap  -->
<script src="js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/jquery-jvectormap-world-mill-en.js"></script>
<!-- chart  
<script src="js/Chart.js"></script>
<script src="js/dashboard2.js"></script>-->

<!-- Sparkline -->
<script src="js/jquery.sparkline.min.js"></script>

<!-- start: Content -->
<div id="content" class="content-wrapper">
    <div class="page-title">
      <div>
        <h1>Dashboard</h1>            
      </div>
      <div>

        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Dashboard</a></li>
        </ul>
      </div>

    </div>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php  if($_SESSION["webadmin"][0]->user_type == 1 ){?>
      <div class="row">
        <?php 
            if ($this->privilegeduser->hasPrivilege("PermissionAddEdit") || $this->privilegeduser->hasPrivilege("PermissionList")) {
          ?>
            <div class="col-md-4" style="margin-bottom: 15px">
                <a href="<?php echo base_url();?>permission" style="font-size: 18px;margin-right: 10px" style="font-size: 18px;margin-right: 10px"><i class="fa fa-lock"></i> Permissions</a>
            </div>
        <?php
          }
        ?>
        <?php 
            if ($this->privilegeduser->hasPrivilege("RoleAddEdit") || $this->privilegeduser->hasPrivilege("RoleList")) {
          ?>
            <div class="col-md-4" style="margin-bottom: 15px">
                <a href="<?php echo base_url();?>roles" style="font-size: 18px;margin-right: 10px"><i class="fa fa-user-circle-o"></i> Roles</a>
            </div>
        <?php
          }
        ?>
        <?php 
            if ($this->privilegeduser->hasPrivilege("UserAddEdit") || $this->privilegeduser->hasPrivilege("UserList")) {
          ?>
            <div class="col-md-4" style="margin-bottom: 15px">
                <a href="<?php echo base_url();?>users" style="font-size: 18px;margin-right: 10px"><i class="fa fa-user"></i> Admin Users</a>
            </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CategoryAddEdit") || $this->privilegeduser->hasPrivilege("CategoryList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>categories" style="font-size: 18px;margin-right: 10px"><i class="fa fa-laptop"></i><span style="font-size: 100%;">Categories</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CourseAddEdit") || $this->privilegeduser->hasPrivilege("CourseList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>coursesmaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-laptop"></i><span style="font-size: 100%;">Courses</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("ThemeAddEdit") || $this->privilegeduser->hasPrivilege("ThemeList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>themesmaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Themes/Months </span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("ZoneAddEdit") || $this->privilegeduser->hasPrivilege("ZoneList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>zonemaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-building"></i><span style="font-size: 100%;">Zone Master </span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CenterAddEdit") || $this->privilegeduser->hasPrivilege("CenterList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>centermaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-building"></i><span style="font-size: 100%;">Center Master </span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CenterUserAddEdit") || $this->privilegeduser->hasPrivilege("CenterUserList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>centerusers" style="font-size: 18px;margin-right: 10px"><i class="fa fa-user"></i><span style="font-size: 100%;">Center Users </span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("TimeTableAddEdit") || $this->privilegeduser->hasPrivilege("TimeTableList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>timetablemaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Timetable Master </span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("AssignTimeTableAddEdit") || $this->privilegeduser->hasPrivilege("AssignTimeTableList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>assigntimetable" style="font-size: 18px;margin-right: 10px"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Assign Timetable </span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit") || $this->privilegeduser->hasPrivilege("DocumentFolderList")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>documentfoldermaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-folder"></i><span style="font-size: 100%;">Document Folder</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CenterTimetableList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
            <a href="<?php echo base_url();?>centertimetables"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Time Tables </span><i class="fa fa-angle-right"></i></a>
          </div>
        <?php }?> 
        <?php 
          if ($this->privilegeduser->hasPrivilege("CenterDocumentFolderList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
            <a href="<?php echo base_url();?>documentfolders"><i class="fa fa-folder"></i><span style="font-size: 100%;">Document Folders </span><i class="fa fa-angle-right"></i></a>
          </div>
        <?php }?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("AcademicYearList") ) {
        ?>
            <div class="col-md-4" style="margin-bottom: 15px">
               <a href="<?php echo base_url();?>academicyear" style="font-size: 18px;margin-right: 10px"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Academic Year Master</span></a>
            </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("BatchList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>batchmaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-laptop"></i><span style="font-size: 100%;">Batch Master</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("GroupList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>groupmaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-users"></i><span style="font-size: 100%;">Group Master</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("FeesComponentList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>feescomponentmaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-money"></i><span style="font-size: 100%;">Fees Component Master</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("FeesLevelList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>feeslevelmaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-money"></i><span style="font-size: 100%;">Fees Level Master</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("FeesMasterList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>feesmaster" style="font-size: 18px;margin-right: 10px"><i class="fa fa-money"></i><span style="font-size: 100%;">Fees Master</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("AssignFeesList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>assignfees" style="font-size: 18px;margin-right: 10px"><i class="fa fa-money"></i><span style="font-size: 100%;">Assign Fees</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CameraList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>camera" style="font-size: 18px;margin-right: 10px"><i class="fa fa-camera"></i><span style="font-size: 100%;">Camera</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("AssignCameraList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>assigncamera" style="font-size: 18px;margin-right: 10px"><i class="fa fa-camera"></i><span style="font-size: 100%;">Assign Camera</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CommunicationCategoriesList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>communicationcategories" style="font-size: 18px;margin-right: 10px"><i class="fa fa-users"></i><span style="font-size: 100%;">Communication Category</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CenterCameraSettingsList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>camerasettings" style="font-size: 18px;margin-right: 10px"><i class="fa fa-camera"></i><span style="font-size: 100%;">Camera Settings</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("CCTVCumulativeSettingsList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>cumulativesettings" style="font-size: 18px;margin-right: 10px"><i class="fa fa-camera"></i><span style="font-size: 100%;">CCTV Cumulative Settings</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("NewsLettersList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>newsletters" style="font-size: 18px;margin-right: 10px"><i class="fa fa-newspaper-o"></i><span style="font-size: 100%;">Newsletters</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("AssignNewsletterList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>assignnewsletter" style="font-size: 18px;margin-right: 10px"><i class="fa fa-newspaper-o"></i><span style="font-size: 100%;">Assign Newsletters</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("SubroutinesList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>subroutines" style="font-size: 18px;margin-right: 10px"><i class="fa fa-clock-o"></i><span style="font-size: 100%;">Subroutines</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("RoutineActionList") ) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>routinesactions" style="font-size: 18px;margin-right: 10px"><i class="fa fa-clock-o"></i><span style="font-size: 100%;">Routine Actions</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("FeedbackList") || $this->privilegeduser->hasPrivilege("FeedbackExport")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>feedbacks" style="font-size: 18px;margin-right: 10px"><i class="fa fa-comment"></i><span style="font-size: 100%;">Feedbacks</span></a>
          </div>
        <?php
          }
        ?>
        <?php 
          if ($this->privilegeduser->hasPrivilege("InquiryList") || $this->privilegeduser->hasPrivilege("InquiryExport")) {
        ?>
          <div class="col-md-4" style="margin-bottom: 15px">
              <a href="<?php echo base_url();?>inquiries" style="font-size: 18px;margin-right: 10px"><i class="fa fa-question"></i><span style="font-size: 100%;">Inquiries</span></a>
          </div>
        <?php
          }
        ?>

          <div class="col-md-4" style="margin-bottom: 15px">
             <a href="<?php echo base_url();?>admission" style="font-size: 18px;margin-right: 10px"><i class="fa fa-graduation-cap"></i><span style="font-size: 100%;">Admission </span></a>
          </div>
      </div>
      <?php }else{?>
        <div class="row">
          <div class="col-md-4" style="margin-bottom: 15px">
            <a href="<?php echo base_url();?>centertimetables"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Time Tables </span><i class="fa fa-angle-right"></i></a>
          </div>
          
          <div class="col-md-4" style="margin-bottom: 15px">
            <a href="<?php echo base_url();?>documentfolders"><i class="fa fa-folder"></i><span style="font-size: 100%;">Document Folders </span><i class="fa fa-angle-right"></i></a>
          </div>
        </div>
        
      <?php }?>
    </section>
    <!-- /.content -->    
    
    
    
    </div>

<!-- end: Content --> 

<script>
$(document).ready(function(){
	$('.recent-msg-txt').click(function(){
		$(this).next().fadeToggle(100);
	});	
});

$(function() {
    $('#toggle-event').change(function() {
      var status = $(this).prop('checked');
      if(status){
        status='yes';
      }else{
        status='no';
      }
      $.ajax({
				url: "<?php echo base_url()?>home/onlineStatus",
				async: false,
        data:{status:status},
				type: "POST",
        dataType:"json",
				success: function(data){
					//return false;
					
					if(data['status'])
					{
						displayMsg("success",data['msg']);
						
					}
					else
					{
						displayMsg("error","Something Went Worng!");
						setTimeout("location.reload(true);",1000);
					}
				},
        error:function(){
          displayMsg("error","Something Went Worng!");
						setTimeout("location.reload(true);",1000);
        }
                
			});
    })
  })
	
</script>

<!-- Vertical Chart -->
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);

function drawMultSeries() {
      var data = new google.visualization.DataTable();
      data.addColumn('timeofday', 'Time of Day');
      data.addColumn('number', 'Motivation Level');
      data.addColumn('number', 'Energy Level');

      data.addRows([
        [{v: [8, 0, 0], f: '8 am'}, 1, .25],
        [{v: [9, 0, 0], f: '9 am'}, 2, .5],
        [{v: [10, 0, 0], f:'10 am'}, 3, 1],
        [{v: [11, 0, 0], f: '11 am'}, 4, 2.25],
        [{v: [12, 0, 0], f: '12 pm'}, 5, 2.25],
        [{v: [13, 0, 0], f: '1 pm'}, 6, 3],
        [{v: [14, 0, 0], f: '2 pm'}, 7, 4],
        [{v: [15, 0, 0], f: '3 pm'}, 8, 5.25],
        [{v: [16, 0, 0], f: '4 pm'}, 9, 7.5],
        [{v: [17, 0, 0], f: '5 pm'}, 10, 10],
      ]);

      var options = {
        title: 'Motivation and Energy Level Throughout the Day',
        hAxis: {
          title: 'Time of Day',
          format: 'h:mm a',
          viewWindow: {
            min: [7, 30, 0],
            max: [17, 30, 0]
          }
        },
        vAxis: {
          title: 'Rating (scale of 1-10)'
        }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }
</script>

<!-- Pie Chart -->
<script>
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'Visitors Report'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

</script>  

<!-- Donut -->
<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'My Daily Activities',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
