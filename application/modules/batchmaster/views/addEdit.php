<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Batch Master</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>batchmaster">Batch Master</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="batch_id" name="batch_id" value="<?php if(!empty($details[0]->batch_id)){echo $details[0]->batch_id;}?>" />
						
						<div class="control-group form-group">
							<label class="control-label" for="category_id">Category*</label>
							<div class="controls">
								<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
									<option value="">Select Category</option>
									<?php 
										if(isset($categories) && !empty($categories)){
											foreach($categories as $cdrow){
												$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
					
						<div class="control-group form-group">
							<label class="control-label"><span>Course*</span></label> 
							<div class="controls">
								<select id="course_id" name="course_id" class="form-control">
									<option value="">Select Course</option>
								</select>
							</div>
						</div>
						
						<div class="control-group form-group">
							<label class="control-label"><span>Zone*</span></label> 
							<div class="controls">
								<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);">
									<option value="">Select Zone</option>
									<?php 
										if(isset($zones) && !empty($zones)){
											foreach($zones as $cdrow){
												$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						
						<div class="control-group form-group">
							<label class="control-label"><span>Center*</span></label> 
							<div class="controls">
								<select id="center_id" name="center_id" class="form-control">
									<option value="">Select Center</option>
								</select>
							</div>
						</div>
						
						<div class="control-group form-group">
							<label class="control-label"><span>Batch Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Name" id="batch_name" name="batch_name" value="<?php if(!empty($details[0]->batch_name)){echo $details[0]->batch_name;}?>" maxlength="50" >
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label"><span>Start Date*</span></label>
							<div class="controls">
								<input type="text" class="form-control required datepicker" placeholder="Select start date" id="start_date" name="start_date" value="<?php if(!empty($details[0]->start_date)){echo date("d-m-Y", strtotime($details[0]->start_date));}?>" >
							</div>
						</div>
						
						<div class="control-group form-group">
							<label class="control-label"><span>End Date*</span></label>
							<div class="controls">
								<input type="text" class="form-control required datepicker" placeholder="Select end date" id="end_date" name="end_date" value="<?php if(!empty($details[0]->end_date)){echo date("d-m-Y", strtotime($details[0]->end_date));}?>" >
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>batchmaster" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->batch_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
		<?php if(!empty($count)){?>
			count = '<?php echo $count;?>';
		<?php }else{?>
			count = '1';
		<?php }?>
		
		<?php if(!empty($count1)){?>
			count1 = '<?php echo $count1;?>';
		<?php }else{?>
			count1 = '1';
		<?php }?>
		
	<?php }?>
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});
});

function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>batchmaster/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}


function getCenters(zone_id,center_id = null)
{;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>batchmaster/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	course_id:{required:true},
	zone_id:{required:true},
	center_id:{required:true},
	batch_name:{required:true, alphanumericwithspace:true},
	start_date:{required:true},
	end_date:{required:true}
	
};
var vMessages = {
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	batch_name:{required:"Please enter batch name."},
	start_date:{required:"Please select start date."},
	end_date:{required:"Please select end date."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>batchmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>batchmaster";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Batch";

 
</script>					
