<?php 
error_reporting(0);
$arrayYesNo = array('Yes'=>'Yes','No'=>'No');
$arrayActive = array('Active'=>'Active','Inactive'=>'Inactive','SoldOut'=>'Sold Out');

?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Student Mood</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>studentmood">Student Mood Master</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="student_mood_id" name="student_mood_id" value="<?php if(!empty($details[0]->student_mood_id)){echo $details[0]->student_mood_id;}?>" />
							
							
								<div class="control-group form-group">
									<label class="control-label"><span>Student Mood*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Student Mood" id="student_mood" name="student_mood" value="<?php if(!empty($details[0]->student_mood)){echo $details[0]->student_mood;}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>studentmood" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->newsletter_id)){
	?>
		//getRegion('<?php echo $details[0]->zone_text; ?>', '<?php echo $details[0]->region_text; ?>');
		//getArea('<?php echo $details[0]->region_text; ?>', '<?php echo $details[0]->area_text; ?>');
		//getCenter('<?php echo $details[0]->area_text; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>
	
});

var vRules = {
	student_mood:{required:true, alphanumericwithspace:true}
	
};
var vMessages = {
	student_mood:{required:"Please enter valid mood."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>studentmood/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>studentmood";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Student Mood";

 
</script>					
